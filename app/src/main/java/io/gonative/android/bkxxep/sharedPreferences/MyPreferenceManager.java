package io.gonative.android.bkxxep.sharedPreferences;

import android.content.Context;
import android.content.SharedPreferences;

import io.gonative.android.bkxxep.utility.Constants;


public class MyPreferenceManager {

    public static SharedPreferences sharedPreferences;
    public static SharedPreferences.Editor editor;

    public static SharedPreferences getInstance(Context mContext) {
        if (sharedPreferences == null) {

            sharedPreferences = mContext.getSharedPreferences(Constants.PARKING_PREFERENCES, Context.MODE_PRIVATE);
            editor = sharedPreferences.edit();
        }
        return sharedPreferences;
    }

    public static void saveString(String key, String value){
        editor.putString(key, value);
        editor.apply();
    }

    public static void getString(String key, String defaultValue){
        sharedPreferences.getString(key, defaultValue);
        editor.apply();
    }
}

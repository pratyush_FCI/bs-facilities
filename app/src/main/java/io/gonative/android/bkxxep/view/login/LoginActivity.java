package io.gonative.android.bkxxep.view.login;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.databinding.DataBindingUtil;
import android.graphics.Rect;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.KeyEvent;
import android.view.MotionEvent;
import android.view.View;
import android.view.inputmethod.EditorInfo;
import android.view.inputmethod.InputMethodManager;
import android.widget.EditText;
import android.widget.TextView;

import com.bridgestone.parkingapp.databinding.ActivityLoginBinding;

import com.crashlytics.android.Crashlytics;

import io.gonative.android.bkxxep.model.login.Login;
import io.gonative.android.bkxxep.utility.CommonUtils;
import io.gonative.android.bkxxep.utility.Constants;
import io.gonative.android.bkxxep.utility.EmailUtils;
import io.gonative.android.bkxxep.utility.KeyboardUtils;
import io.gonative.android.bkxxep.utility.NetworkUtils;
import io.gonative.android.bkxxep.utility.RetrofitUtility;
import io.gonative.android.bkxxep.view.otp.OtpActivity;
import io.fabric.sdk.android.Fabric;
import io.reactivex.Observable;
import io.reactivex.Observer;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.Disposable;
import io.reactivex.schedulers.Schedulers;

public class LoginActivity extends AppCompatActivity implements View.OnClickListener {

    private static final String TAG = LoginActivity.class.getSimpleName();
    private Context mContext;

    private ActivityLoginBinding binding;
    private ProgressDialog progressDialog;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Fabric.with(this, new Crashlytics());
//        setContentView(R.layout.activity_login);
        binding = DataBindingUtil.setContentView(this, io.gonative.android.bkxxep.R.layout.activity_login);
        mContext = this;
        binding.btLogin.setOnClickListener(this);
        binding.btLoginAdmin.setOnClickListener(this);

        binding.etEmail.setOnEditorActionListener(new TextView.OnEditorActionListener() {
            @Override
            public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
                boolean handled = false;
                if (actionId == EditorInfo.IME_ACTION_DONE) {
                    /* Write your logic here that will be executed when user taps next button */
                    Log.d(TAG, "onEditorAction: ");
                    KeyboardUtils.hideSoftKeyboard(mContext);
                    String email = binding.etEmail.getText().toString().trim();


                    if (EmailUtils.isEmailValid(email)) {
                        if (EmailUtils.getEmailDomain(email).equalsIgnoreCase(Constants.BRIDGESTONE_DOMAIN) || EmailUtils.getEmailDomain(email).equalsIgnoreCase(Constants.FUJITSU_DOMAIN) || EmailUtils.getEmailDomain(email).equalsIgnoreCase(Constants.FIRSTSTOP_DOMAIN)) {

                            if (NetworkUtils.isNetworkAvailable(mContext, binding.llMain)) {
                                callLoginApi();
                            }

                        } else {
                            CommonUtils.showSnackBar(getString(io.gonative.android.bkxxep.R.string.please_enter_bridgestone_domain), binding.llMain);
                        }
                    } else {
                        CommonUtils.showSnackBar(getString(io.gonative.android.bkxxep.R.string.please_enter_valid_email), binding.llMain);
                    }
                }
                return true;
            }
        });

    }

    @Override
    public void onClick(View v) {
        Intent intent = null;
        switch (v.getId()) {
            case io.gonative.android.bkxxep.R.id.btLogin:
                String email = binding.etEmail.getText().toString().trim();
                KeyboardUtils.hideSoftKeyboard(mContext);
//                intent = new Intent(mContext, MainActivity.class);
//                startActivity(intent);

                if (EmailUtils.isEmailValid(email)) {
                    if (EmailUtils.getEmailDomain(email).equalsIgnoreCase(Constants.BRIDGESTONE_DOMAIN) ||
                            EmailUtils.getEmailDomain(email).equalsIgnoreCase(Constants.FUJITSU_DOMAIN)||EmailUtils.getEmailDomain(email).equalsIgnoreCase(Constants.FIRSTSTOP_DOMAIN)) {

                        if (NetworkUtils.isNetworkAvailable(mContext, binding.llMain)) {
                            callLoginApi();
                        }

                    } else {
                        CommonUtils.showSnackBar(getString(io.gonative.android.bkxxep.R.string.please_enter_bridgestone_domain), binding.llMain);
                    }
                } else {
                    CommonUtils.showSnackBar(getString(io.gonative.android.bkxxep.R.string.please_enter_valid_email), binding.llMain);
                }
                break;

            case io.gonative.android.bkxxep.R.id.btLoginAdmin:
                intent = new Intent(mContext, OtpActivity.class);
                startActivity(intent);
                break;
        }
    }

    private void callLoginApi() {
        Observable<Login> observable = RetrofitUtility.getClient().login(binding.etEmail.getText().toString().toLowerCase().trim());
        progressDialog = CommonUtils.showProgressDialog(mContext);
        try {
            observable.subscribeOn(Schedulers.io())
                    .observeOn(AndroidSchedulers.mainThread())
                    .subscribe(new Observer<Login>() {
                        @Override
                        public void onSubscribe(Disposable d) {
                            Log.d(TAG, "onSubscribe:");
                        }

                        @Override
                        public void onNext(Login value) {
                            Log.d(TAG, "onNext:");
                            if (value.getStatus().equalsIgnoreCase("OK")) {
                                CommonUtils.dismissProgressDialog(progressDialog);
                                Intent intent = new Intent(mContext, OtpActivity.class);
                                intent.putExtra(Constants.EID, binding.etEmail.getText().toString().trim());
                                startActivity(intent);
                            } else {
                                CommonUtils.showSnackBar(value.getStatus(), binding.llMain);
                            }
                        }

                        @Override
                        public void onError(Throwable e) {
                            Log.d(TAG, "onError:" + e.getMessage());
                            CommonUtils.dismissProgressDialog(progressDialog);
                            CommonUtils.showSnackBar(getString(io.gonative.android.bkxxep.R.string.something_went_wrong), binding.llMain);

                        }

                        @Override
                        public void onComplete() {
                            Log.d(TAG, "onComplete:");
                            CommonUtils.dismissProgressDialog(progressDialog);
                        }
                    });
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public boolean dispatchTouchEvent(MotionEvent ev) {
        if (ev.getAction() == MotionEvent.ACTION_UP) {
            final View view = getCurrentFocus();

            if (view != null) {
                final boolean consumed = super.dispatchTouchEvent(ev);

                final View viewTmp = getCurrentFocus();
                final View viewNew = viewTmp != null ? viewTmp : view;

                if (viewNew.equals(view)) {
                    final Rect rect = new Rect();
                    final int[] coordinates = new int[2];

                    view.getLocationOnScreen(coordinates);

                    rect.set(coordinates[0], coordinates[1], coordinates[0] + view.getWidth(), coordinates[1] + view.getHeight());

                    final int x = (int) ev.getX();
                    final int y = (int) ev.getY();

                    if (rect.contains(x, y)) {
                        return consumed;
                    }
                } else if (viewNew instanceof EditText) {
                    return consumed;
                }

                final InputMethodManager inputMethodManager = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);

                inputMethodManager.hideSoftInputFromWindow(viewNew.getWindowToken(), 0);

                viewNew.clearFocus();

                return consumed;
            }
        }

        return super.dispatchTouchEvent(ev);
    }

    private void hideSoftKeyboard() {
        InputMethodManager inputManager = (InputMethodManager)
                getSystemService(Context.INPUT_METHOD_SERVICE);
        if (inputManager.isAcceptingText()) {
            inputManager.hideSoftInputFromWindow(getCurrentFocus().getWindowToken(),
                    InputMethodManager.HIDE_NOT_ALWAYS);
        }
    }

}

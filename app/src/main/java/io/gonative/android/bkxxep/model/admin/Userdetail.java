package io.gonative.android.bkxxep.model.admin;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Userdetail {

    @SerializedName("_id")
    @Expose
    private String id;
    @SerializedName("eid")
    @Expose
    private String eid;
    @SerializedName("username")
    @Expose
    private String username;
    @SerializedName("parkingid")
    @Expose
    private String parkingid;
    @SerializedName("role")
    @Expose
    private String role;
    @SerializedName("password")
    @Expose
    private String password;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getEid() {
        return eid;
    }

    public void setEid(String eid) {
        this.eid = eid;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getParkingid() {
        return parkingid;
    }

    public void setParkingid(String parkingid) {
        this.parkingid = parkingid;
    }

    public String getRole() {
        return role;
    }

    public void setRole(String role) {
        this.role = role;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }
}

package io.gonative.android.bkxxep.application;

import android.app.Application;


import io.gonative.android.bkxxep.utility.Constants;
import io.socket.client.IO;
import io.socket.client.Socket;

import java.net.URISyntaxException;

public class ParkingApplication extends Application {


    private Socket mSocket;
    {
        try {
            mSocket = IO.socket(Constants.BASE_URL);

        } catch (URISyntaxException e) {
            throw new RuntimeException(e);
        }
    }

    public Socket getSocket() {
        return mSocket;
    }
}

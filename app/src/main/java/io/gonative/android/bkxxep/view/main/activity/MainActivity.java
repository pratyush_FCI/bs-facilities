package io.gonative.android.bkxxep.view.main.activity;

import android.annotation.SuppressLint;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;

import com.bridgestone.parkingapp.databinding.ActivityMainBinding;


import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;

import io.gonative.android.bkxxep.model.freeParking.FreeParking;
import io.gonative.android.bkxxep.model.occupyParking.OccupyParking;
import io.gonative.android.bkxxep.utility.CommonUtils;
import io.gonative.android.bkxxep.utility.Constants;
import io.gonative.android.bkxxep.utility.NetworkUtils;
import io.gonative.android.bkxxep.utility.RetrofitUtility;
import io.gonative.android.bkxxep.utility.SharedPreferencesUtils;
import io.gonative.android.bkxxep.view.admin.activity.AdminActivity;
import io.gonative.android.bkxxep.view.main.fragment.DatePickerFragment;
import io.gonative.android.bkxxep.view.main.fragment.FreeUpParkingDialogFragment;
import io.gonative.android.bkxxep.view.parking.ParkingActivity;
import io.reactivex.Observable;
import io.reactivex.Observer;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.Disposable;
import io.reactivex.schedulers.Schedulers;

import static com.bridgestone.parkingapp.view.admin.adapter.UsersRecyclerAdapter.REQUEST_EDIT_CODE;

public class MainActivity extends AppCompatActivity implements View.OnClickListener, DatePickerFragment.DateInterface, FreeUpParkingDialogFragment.SubmitDate {

    private static final String TAG = MainActivity.class.getSimpleName();
    private Context mContext;
    private FreeUpParkingDialogFragment fragment;
    private ActivityMainBinding mainBinding;
    private ProgressDialog progressDialog;
    private boolean parkingOccupied;
    public static final int REQUEST_ADMIN_CODE = 101;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
//        setContentView(R.layout.activity_main);
        mainBinding = DataBindingUtil.setContentView(this, io.gonative.android.bkxxep.R.layout.activity_main);
        mContext = this;
        initializeUi();
    }

    @SuppressLint("SetTextI18n")
    private void initializeUi() {
        if (SharedPreferencesUtils.getRole(mContext).equalsIgnoreCase("admin")) {
            mainBinding.btAdminPanel.setVisibility(View.VISIBLE);
            Log.d(TAG, "initializeUi: admin visible");
        } else {
            mainBinding.btAdminPanel.setVisibility(View.GONE);
            Log.d(TAG, "initializeUi: admin gone");
        }

        if (!SharedPreferencesUtils.getParkingId(mContext).isEmpty()) {
            mainBinding.btFreeParking.setVisibility(View.VISIBLE);
        } else {
            mainBinding.btFreeParking.setVisibility(View.GONE);
        }
        if (!SharedPreferencesUtils.getEndDate(mContext).isEmpty()) {

            checkDate(SharedPreferencesUtils.getEndDate(mContext));
        }
        parkingOccupied = SharedPreferencesUtils.getIfParkingOccupied(mContext);
        Log.d(TAG, "initializeUi: " + parkingOccupied);
        if (parkingOccupied) {
            mainBinding.btFreeParking.setText(getString(io.gonative.android.bkxxep.R.string.free_up_parking));
        } else {
            mainBinding.btFreeParking.setText(getString(io.gonative.android.bkxxep.R.string.occupy_parking));
        }

        if (SharedPreferencesUtils.getIsGuestUser(mContext).isEmpty()) {

            mainBinding.tvUserName.setText(getString(io.gonative.android.bkxxep.R.string.hello) + " " + SharedPreferencesUtils.getUsername(mContext));
        } else {
            mainBinding.tvUserName.setText(getString(io.gonative.android.bkxxep.R.string.hello) + " " + getString(io.gonative.android.bkxxep.R.string.guest_user));
        }

        mainBinding.btFreeParking.setOnClickListener(this);
        mainBinding.btViewParking.setOnClickListener(this);
        mainBinding.btAdminPanel.setOnClickListener(this);
        mainBinding.tvLogout.setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {

        switch (v.getId()) {
            case io.gonative.android.bkxxep.R.id.btFreeParking:
                if (parkingOccupied) {
                    if (fragment == null) {
                        fragment = FreeUpParkingDialogFragment.newInstance();
                    }

                    if (!fragment.isAdded()) {
                        fragment.show(getSupportFragmentManager(), "");
                    }
                } else {
                    if (NetworkUtils.isNetworkAvailable(mContext, mainBinding.llMain)) {
                        callOccupyParkingApi();
                    }
                }
                break;

            case io.gonative.android.bkxxep.R.id.btViewParking:
                startActivity(new Intent(this, ParkingActivity.class));
                break;
            case io.gonative.android.bkxxep.R.id.btAdminPanel:
                startActivityForResult(new Intent(this, AdminActivity.class), REQUEST_ADMIN_CODE);
                Log.d("MainActivity", "onClick: ");
                break;
            case io.gonative.android.bkxxep.R.id.tvLogout:
                SharedPreferencesUtils.logout(this);
                break;

        }
    }

    private void callOccupyParkingApi() {

        Observable<OccupyParking> observable = RetrofitUtility.getClient().occupyParking(SharedPreferencesUtils.getParkingId(mContext));

        progressDialog = CommonUtils.showProgressDialog(mContext);
        try {
            observable.subscribeOn(Schedulers.io())
                    .observeOn(AndroidSchedulers.mainThread())
                    .subscribe(new Observer<OccupyParking>() {
                        @Override
                        public void onSubscribe(Disposable d) {
                            Log.d(TAG, "onSubscribe:");
                        }

                        @Override
                        public void onNext(OccupyParking value) {
                            Log.d(TAG, "onNext:");
                            CommonUtils.dismissProgressDialog(progressDialog);
                            if (value.getStatus().equalsIgnoreCase("updated successfully")) {
                                CommonUtils.showSnackBar(value.getStatus(), mainBinding.llMain);
                                parkingOccupied = true;
                                mainBinding.btFreeParking.setText(getString(io.gonative.android.bkxxep.R.string.free_up_parking));
                                SharedPreferences sharedPreferences = mContext.getSharedPreferences(Constants.PARKING_PREFERENCES, MODE_PRIVATE);
                                SharedPreferences.Editor editor = sharedPreferences.edit();
                                editor.putBoolean(Constants.PARKINGOCCUPIED, true);
                                editor.apply();
                                editor.commit();
                            } else {
                                CommonUtils.showSnackBar(value.getStatus(), mainBinding.llMain);
                            }
                        }

                        @Override
                        public void onError(Throwable e) {
                            Log.d(TAG, "onError:" + e.getMessage());
                            CommonUtils.dismissProgressDialog(progressDialog);
                            CommonUtils.showSnackBar(getString(io.gonative.android.bkxxep.R.string.something_went_wrong), mainBinding.llMain);

                        }

                        @Override
                        public void onComplete() {
                            Log.d(TAG, "onComplete:");
                            CommonUtils.dismissProgressDialog(progressDialog);

                        }
                    });
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public void sendDate(String date, String tag) {
        if (fragment != null) {
            fragment.setdate(date, tag);
        }
    }

    @Override
    public void submit(String startDate, String endDate) {
        Log.d(TAG, "submit: " + startDate + "-" + endDate);
        if (NetworkUtils.isNetworkAvailable(mContext, mainBinding.llMain)) {
            callFreeParkingApi(startDate, endDate);
            Log.d(TAG, "submit: callFreeParkingApi");
        }
    }

    private void callFreeParkingApi(String startDate, String endDate) {

        HashMap<String, String> map = new HashMap<>();
        map.put(Constants.PARKINGID, SharedPreferencesUtils.getParkingId(mContext));
        map.put(Constants.STARTDATE, startDate);
        map.put(Constants.ENDDATE, endDate);
        Observable<FreeParking> observable = RetrofitUtility.getClient().freeParking(map);

        progressDialog = CommonUtils.showProgressDialog(mContext);
        try {
            observable.subscribeOn(Schedulers.io())
                    .observeOn(AndroidSchedulers.mainThread())
                    .subscribe(new Observer<FreeParking>() {
                        @Override
                        public void onSubscribe(Disposable d) {
                            Log.d(TAG, "onSubscribe:");
                        }

                        @Override
                        public void onNext(FreeParking value) {
                            Log.d(TAG, "onNext:");
                            CommonUtils.dismissProgressDialog(progressDialog);
                            if (value.getUserdetail().getNModified().equals(1)) {
                                CommonUtils.showSnackBar("Parking free", mainBinding.llMain);
                                mainBinding.btFreeParking.setText(getString(io.gonative.android.bkxxep.R.string.occupy_parking));
                                parkingOccupied = false;
                                SharedPreferences sharedPreferences = mContext.getSharedPreferences(Constants.PARKING_PREFERENCES, MODE_PRIVATE);
                                SharedPreferences.Editor editor = sharedPreferences.edit();
                                editor.putBoolean(Constants.PARKINGOCCUPIED, false);
                                editor.apply();
                                editor.commit();
                            }
                        }

                        @Override
                        public void onError(Throwable e) {
                            Log.d(TAG, "onError:" + e.getMessage());
                            CommonUtils.dismissProgressDialog(progressDialog);
                            CommonUtils.showSnackBar(getString(io.gonative.android.bkxxep.R.string.something_went_wrong), mainBinding.llMain);
                        }

                        @Override
                        public void onComplete() {
                            Log.d(TAG, "onComplete:");
                            CommonUtils.dismissProgressDialog(progressDialog);
                        }
                    });
        } catch (Exception e) {
            e.printStackTrace();
        }
    }


    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (resultCode == RESULT_OK) {
            if (requestCode == REQUEST_ADMIN_CODE) {
                Log.d(TAG, "onActivityResult: ");
                initializeUi();
            }
        }
    }

    private boolean checkDate(String date) {
        SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy");
        Date endDate = null, curDate = null;
        boolean catalog_outdated;
        Log.d(TAG, "checkDate: enddate " + date);


        Calendar cal = Calendar.getInstance();

// go back one days
//        cal.add(Calendar.DAY_OF_YEAR, -1);
        SimpleDateFormat formatter = new SimpleDateFormat("dd/MM/yyyy");
        String dateCurrent = formatter.format(cal.getTime());
        try {
            endDate = sdf.parse(date);
            curDate = formatter.parse(dateCurrent);
        } catch (ParseException e) {
            e.printStackTrace();
        }


        Log.d(TAG, "checkDate curdate: " + curDate);
        SharedPreferences sharedPreferences = mContext.getSharedPreferences(Constants.PARKING_PREFERENCES, MODE_PRIVATE);
        SharedPreferences.Editor editor = sharedPreferences.edit();
        // Today date is after specified date
        if (curDate.after(endDate)) {
            catalog_outdated = true;
            editor.putBoolean(Constants.PARKINGOCCUPIED, true);
            editor.putString(Constants.STARTDATE, "");
            editor.putString(Constants.ENDDATE, "");

            Log.d(TAG, "checkDate: " + true);
        } else {
            catalog_outdated = false;
            Log.d(TAG, "checkDate: " + false);
            editor.putBoolean(Constants.PARKINGOCCUPIED, false);
        }
        editor.apply();
        editor.commit();
        return catalog_outdated;
    }
}

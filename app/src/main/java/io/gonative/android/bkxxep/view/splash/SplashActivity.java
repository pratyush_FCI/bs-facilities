package io.gonative.android.bkxxep.view.splash;

import android.content.Context;
import android.content.Intent;
import android.databinding.DataBindingUtil;
import android.os.Handler;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;

import com.bridgestone.parkingapp.R;
import com.bridgestone.parkingapp.databinding.ActivitySplashBinding;
import com.bridgestone.parkingapp.utility.SharedPreferencesUtils;
import com.bridgestone.parkingapp.view.login.LoginActivity;
import com.bridgestone.parkingapp.view.main.activity.MainActivity;

import io.gonative.android.bkxxep.utility.SharedPreferencesUtils;

public class SplashActivity extends AppCompatActivity {


    private Context mContext;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        ActivitySplashBinding binding = DataBindingUtil.setContentView(this, io.gonative.android.bkxxep.R.layout.activity_splash);

        mContext = this;
        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                if (SharedPreferencesUtils.getUserId(mContext).isEmpty() && SharedPreferencesUtils.getIsGuestUser(mContext).isEmpty()) {
                    startActivity(new Intent(mContext, LoginActivity.class));
                    finish();
                } else {
                    startActivity(new Intent(mContext, MainActivity.class));
                    finish();
                }
            }
        }, 3000);
    }
}

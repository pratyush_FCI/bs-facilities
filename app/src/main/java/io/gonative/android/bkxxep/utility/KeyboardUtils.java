package io.gonative.android.bkxxep.utility;

import android.app.Activity;
import android.content.Context;
import android.view.View;
import android.view.inputmethod.InputMethodManager;

public class KeyboardUtils {
    public static void hideSoftKeyboard(Context context) {
        /*InputMethodManager inputManager = (InputMethodManager)
               context.getSystemService(Context.INPUT_METHOD_SERVICE);
        if (inputManager.isAcceptingText()) {
            inputManager.hideSoftInputFromWindow(((Activity)context).getCurrentFocus().getWindowToken(),
                    InputMethodManager.HIDE_NOT_ALWAYS);
        }*/

        InputMethodManager inputManager = (InputMethodManager) (((Activity)context).getSystemService(
                Context.INPUT_METHOD_SERVICE));
        View focusedView = (((Activity)context).getCurrentFocus());
        /*
         * If no view is focused, an NPE will be thrown
         *
         * Maxim Dmitriev
         */
        if (focusedView != null) {
            inputManager.hideSoftInputFromWindow(focusedView.getWindowToken(),
                    InputMethodManager.HIDE_NOT_ALWAYS);
        }
    }
}

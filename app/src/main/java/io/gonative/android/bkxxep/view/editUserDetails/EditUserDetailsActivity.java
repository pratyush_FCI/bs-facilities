package io.gonative.android.bkxxep.view.editUserDetails;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.databinding.DataBindingUtil;
import android.graphics.Rect;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.KeyEvent;
import android.view.MotionEvent;
import android.view.View;
import android.view.inputmethod.EditorInfo;
import android.view.inputmethod.InputMethodManager;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.TextView;

import com.bridgestone.parkingapp.databinding.ActivityUserDetailsBinding;


import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import io.gonative.android.bkxxep.model.requestBody.updateUser.UpdateUserDetailsRequestBody;
import io.gonative.android.bkxxep.model.updateUserDetails.UpdateUserDetails;
import io.gonative.android.bkxxep.utility.CommonUtils;
import io.gonative.android.bkxxep.utility.Constants;
import io.gonative.android.bkxxep.utility.KeyboardUtils;
import io.gonative.android.bkxxep.utility.NetworkUtils;
import io.gonative.android.bkxxep.utility.RetrofitUtility;
import io.gonative.android.bkxxep.utility.SharedPreferencesUtils;
import io.reactivex.Observable;
import io.reactivex.Observer;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.Disposable;
import io.reactivex.schedulers.Schedulers;

public class EditUserDetailsActivity extends AppCompatActivity implements View.OnClickListener {

    private static final String TAG = EditUserDetailsActivity.class.getSimpleName();
    private String id, eid;
    private Context mContext;

    private ActivityUserDetailsBinding binding;
    private ArrayList<String> parkingIdList;
    private String selectedRole, previousRole, username, parkingId;
    private boolean itemSelected;
    private ProgressDialog progressDialog;
    private boolean isItemSelected;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        binding = DataBindingUtil.setContentView(this, io.gonative.android.bkxxep.R.layout.activity_user_details);
        mContext = this;

        id = getIntent().getStringExtra(Constants.ID);
        eid = getIntent().getStringExtra(Constants.EID);
        previousRole = getIntent().getStringExtra("role");
        username = getIntent().getStringExtra(Constants.USERNAME);
        parkingId = getIntent().getStringExtra(Constants.PARKINGID);

        Log.d(TAG, "onCreate: " + id);

        binding.toolbar.tvToolbarTitle.setText(getString(io.gonative.android.bkxxep.R.string.edit_details));
        binding.toolbar.ivBack.setOnClickListener(this);
        binding.etEmail.setHint(eid);
        binding.etUserName.setText(username);
//        binding.etParkingId.setText(parkingId);
        binding.etEmail.setKeyListener(null);
        binding.btUpdate.setOnClickListener(this);
        setSpinnerItems();

        binding.etUserName.setOnEditorActionListener(new TextView.OnEditorActionListener() {
            @Override
            public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
                boolean handled = false;
                if (actionId == EditorInfo.IME_ACTION_DONE) {
                    /* Write your logic here that will be executed when user taps next button */
                    Log.d(TAG, "onEditorAction: ");
                    KeyboardUtils.hideSoftKeyboard(mContext);
                    if (verifyUpdateDetails() && NetworkUtils.isNetworkAvailable(mContext, binding.llMain)) {
                        callUpdateUserDetailsApi();
                    }
                }
                return true;
            }
        });
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case io.gonative.android.bkxxep.R.id.ivBack:
                onBackPressed();
                break;
            case io.gonative.android.bkxxep.R.id.btUpdate:
                KeyboardUtils.hideSoftKeyboard(mContext);
                if (verifyUpdateDetails() && NetworkUtils.isNetworkAvailable(mContext, binding.llMain)) {
                    callUpdateUserDetailsApi();
                }
                break;
        }
    }


    @Override
    public boolean dispatchTouchEvent(MotionEvent ev) {
        if (ev.getAction() == MotionEvent.ACTION_UP) {
            final View view = getCurrentFocus();

            if (view != null) {
                final boolean consumed = super.dispatchTouchEvent(ev);

                final View viewTmp = getCurrentFocus();
                final View viewNew = viewTmp != null ? viewTmp : view;

                if (viewNew.equals(view)) {
                    final Rect rect = new Rect();
                    final int[] coordinates = new int[2];

                    view.getLocationOnScreen(coordinates);

                    rect.set(coordinates[0], coordinates[1], coordinates[0] + view.getWidth(), coordinates[1] + view.getHeight());

                    final int x = (int) ev.getX();
                    final int y = (int) ev.getY();

                    if (rect.contains(x, y)) {
                        return consumed;
                    }
                } else if (viewNew instanceof EditText) {
                    return consumed;
                }

                final InputMethodManager inputMethodManager = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);

                inputMethodManager.hideSoftInputFromWindow(viewNew.getWindowToken(), 0);

                viewNew.clearFocus();

                return consumed;
            }
        }

        return super.dispatchTouchEvent(ev);
    }

    private void callUpdateUserDetailsApi() {
        UpdateUserDetailsRequestBody body = new UpdateUserDetailsRequestBody();
        body.setId(id);
        HashMap<String, String> map = new HashMap<>();
        map.put(Constants.ID, id);

        if (!binding.etUserName.getText().toString().trim().isEmpty()) {
            body.setUsername(binding.etUserName.getText().toString().trim());
            if (id.equalsIgnoreCase(SharedPreferencesUtils.getUserId(mContext))) {
                SharedPreferences sharedPreferences = mContext.getSharedPreferences(Constants.PARKING_PREFERENCES, MODE_PRIVATE);
                SharedPreferences.Editor editor = sharedPreferences.edit();
                editor.putString(Constants.USERNAME, binding.etUserName.getText().toString().trim());
                editor.apply();
                editor.commit();
            }
            map.put(Constants.USERNAME, binding.etUserName.getText().toString().trim());
        }

        if (!selectedRole.isEmpty()) {
            body.setRole(selectedRole);
            map.put(Constants.ROLE, selectedRole);
        }


        Observable<UpdateUserDetails> observable = RetrofitUtility.getClient().updateUserDetails(map);
        progressDialog = CommonUtils.showProgressDialog(mContext);
        try {
            observable.subscribeOn(Schedulers.io())
                    .observeOn(AndroidSchedulers.mainThread())
                    .subscribe(new Observer<UpdateUserDetails>() {
                        @Override
                        public void onSubscribe(Disposable d) {
                            Log.d(TAG, "onSubscribe: ");
                        }

                        @Override
                        public void onNext(UpdateUserDetails value) {
                            Log.d(TAG, "onNext: ");
                            if (value.getStatus().equalsIgnoreCase("Record Already exist")) {
                                CommonUtils.showSnackBar(value.getStatus(), binding.llMain);
                                Log.d(TAG, "onNext: Record Already exist");
                            } else {
                                CommonUtils.dismissProgressDialog(progressDialog);
                                CommonUtils.showSnackBar(value.getStatus(), binding.llMain);
                                Log.d(TAG, "onNext: Record Already not exist");

                                Intent intent = new Intent();
                                setResult(Activity.RESULT_OK, intent);
                                finish();

                                if (!binding.etUserName.getText().toString().trim().isEmpty()) {
                                    if (id.equalsIgnoreCase(SharedPreferencesUtils.getUserId(mContext))) {
                                        SharedPreferences sharedPreferences = mContext.getSharedPreferences(Constants.PARKING_PREFERENCES, MODE_PRIVATE);
                                        SharedPreferences.Editor editor = sharedPreferences.edit();
                                        editor.putString(Constants.USERNAME, binding.etUserName.getText().toString().trim());
                                        editor.apply();
                                        editor.commit();
                                    }
                                }

                            }
                        }

                        @Override
                        public void onError(Throwable e) {
                            Log.d(TAG, "onError: " + e.getMessage());
                            CommonUtils.dismissProgressDialog(progressDialog);
                            CommonUtils.showSnackBar(getString(io.gonative.android.bkxxep.R.string.something_went_wrong), binding.llMain);

                        }

                        @Override
                        public void onComplete() {
                            Log.d(TAG, "onComplete: ");
                            CommonUtils.dismissProgressDialog(progressDialog);

                        }
                    });
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void setSpinnerItems() {

        parkingIdList = new ArrayList<>();
        for (int i = 1; i < 111; i++) {
            if (i == 57) {

            } else {
                parkingIdList.add(String.valueOf(i));
            }
        }
        Log.d(TAG, "setSpinnerItems: " + parkingIdList.size());
        List<String> list = new ArrayList<String>();
        list.add(getString(io.gonative.android.bkxxep.R.string.select_role));
        list.add(getString(io.gonative.android.bkxxep.R.string.admin));
        list.add(getString(io.gonative.android.bkxxep.R.string.regular));
        ArrayAdapter<String> dataAdapter = new ArrayAdapter<String>(this,
                android.R.layout.simple_spinner_item, list);
        dataAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        binding.spRole.setAdapter(dataAdapter);
        if (previousRole.equalsIgnoreCase("admin")){
            binding.spRole.setSelection(1);

        }else {
            binding.spRole.setSelection(2);

        }
        binding.spRole.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                Log.d(TAG, "onItemSelected: " + position);
                if (isItemSelected) {
                    if (position == 0) {
                        selectedRole = "";
                    } else {
                        selectedRole = list.get(position).toLowerCase();
                        Log.d(TAG, "setSpinnerItems: " + selectedRole);
                    }
                } else {
                    selectedRole = "";
                    isItemSelected = true;
                }

            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {
                Log.d(TAG, "onNothingSelected: ");
            }
        });
    }

    private boolean verifyUpdateDetails() {
        /*if (previousRole.equalsIgnoreCase("Admin") && selectedRole.equalsIgnoreCase("regular") &&
                binding.etParkingId.getText().toString().trim().isEmpty() && parkingId!=null && parkingId.isEmpty()) {
            CommonUtils.showSnackBar(getString(R.string.parking_id_is_necessary_for_regular_users), binding.llMain);
            return false;
        }

        String pid = binding.etParkingId.getText().toString().trim();
        if (!pid.isEmpty() && !parkingIdList.contains(pid)) {
            CommonUtils.showSnackBar(getString(R.string.please_enter_valid_parking_id), binding.llMain);
            return false;
        }*/

        if (binding.etUserName.getText().toString().isEmpty()  && selectedRole.isEmpty()) {
            CommonUtils.showSnackBar("Details can't be empty", binding.llMain);
            return false;
        }

        /*if (selectedRole.equalsIgnoreCase("regular") && binding.etParkingId.getText().toString().trim().isEmpty()) {
            CommonUtils.showSnackBar(getString(R.string.parking_id_is_necessary_for_regular_users), binding.llMain);
            return false;
        }*/
        return true;
    }

    @Override
    public void onBackPressed() {
        Intent intent = new Intent();
        setResult(Activity.RESULT_CANCELED, intent);
        super.onBackPressed();
    }
}

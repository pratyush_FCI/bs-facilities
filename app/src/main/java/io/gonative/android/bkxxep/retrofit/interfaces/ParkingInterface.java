package io.gonative.android.bkxxep.retrofit.interfaces;



import java.util.HashMap;

import io.gonative.android.bkxxep.model.addUser.AddUser;
import io.gonative.android.bkxxep.model.admin.DeleteUserDetails;
import io.gonative.android.bkxxep.model.admin.Users;
import io.gonative.android.bkxxep.model.freeParking.FreeParking;
import io.gonative.android.bkxxep.model.login.Login;
import io.gonative.android.bkxxep.model.occupyParking.OccupyParking;
import io.gonative.android.bkxxep.model.otp.VerifyOtp;
import io.gonative.android.bkxxep.model.parking.ParkingDetails;
import io.gonative.android.bkxxep.model.updateUserDetails.UpdateUserDetails;
import io.gonative.android.bkxxep.utility.Constants;
import io.reactivex.Observable;
import retrofit2.Response;
import retrofit2.http.Field;
import retrofit2.http.FieldMap;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.GET;
import retrofit2.http.Headers;
import retrofit2.http.POST;
import retrofit2.http.Query;
import retrofit2.http.QueryMap;


public interface ParkingInterface {

    @GET("getparkingdetail")
    Observable<Response<ParkingDetails>> getParkingDetails(@Query("date") String date);

    @GET("getuserdetail")
    Observable<Users> getUserDetails();

    @GET("loginVerfication")
    Observable<Login> login(@Query(Constants.EID) String email);

    @FormUrlEncoded
    @POST("insertuserdetail")
    Observable<AddUser> addNewUser(@Field(Constants.EID) String email,
                                   @Field(Constants.USERNAME) String userName,
                                   @Field(Constants.PARKINGID) String parkingId,
                                   @Field(Constants.ROLE) String role);

    /*@Headers("Content-Type: application/x-www-form-urlencoded")
    @HTTP(method = "DELETE", path = "deleteuserdetail", hasBody = true)
    Observable<DeleteUserDetails> deleteUser(@Body DeleteUserDetailsRequestBody body);*/

    @Headers("Content-Type: application/x-www-form-urlencoded")
    @FormUrlEncoded
    @POST("deleteuserdetail")
    Observable<DeleteUserDetails> deleteUser(@Field("id") String id);

    @FormUrlEncoded
    @POST("updateuserdetail")
    Observable<UpdateUserDetails> updateUserDetails(@FieldMap HashMap<String, String> map);

    /*(*//*Constants.ID) String id,
                                              @Field(Constants.EID) String eid,
                                              @Field(Constants.USERNAME) String username,
                                              @Field(Constants.ROLE) String role,
                                              @Field(Constants.PARKINGID) String pa);*/
    @FormUrlEncoded
    @POST("freeparking")
    Observable<FreeParking> freeParking(@FieldMap HashMap<String, String> map);

    @FormUrlEncoded
    @POST("occupyparking")
    Observable<OccupyParking> occupyParking(@Field(Constants.PARKINGID) String parkingId);

    @GET("verifyOTP")
    Observable<VerifyOtp> verifyOtp(@QueryMap HashMap<String, String> map);
}

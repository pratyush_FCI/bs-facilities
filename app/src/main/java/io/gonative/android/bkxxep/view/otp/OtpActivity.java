package io.gonative.android.bkxxep.view.otp;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.databinding.DataBindingUtil;
import android.graphics.Rect;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.KeyEvent;
import android.view.MotionEvent;
import android.view.View;
import android.view.inputmethod.EditorInfo;
import android.view.inputmethod.InputMethodManager;
import android.widget.EditText;
import android.widget.TextView;

import com.bridgestone.parkingapp.R;
import com.bridgestone.parkingapp.databinding.ActivityLoginAdminBinding;
import com.bridgestone.parkingapp.model.otp.Userdetail;
import com.bridgestone.parkingapp.model.otp.VerifyOtp;
import com.bridgestone.parkingapp.sharedPreferences.MyPreferenceManager;
import com.bridgestone.parkingapp.utility.CommonUtils;
import com.bridgestone.parkingapp.utility.Constants;
import com.bridgestone.parkingapp.utility.EmailUtils;
import com.bridgestone.parkingapp.utility.KeyboardUtils;
import com.bridgestone.parkingapp.utility.NetworkUtils;
import com.bridgestone.parkingapp.utility.RetrofitUtility;
import com.bridgestone.parkingapp.view.main.activity.MainActivity;

import java.util.HashMap;

import io.reactivex.Observable;
import io.reactivex.Observer;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.Disposable;
import io.reactivex.schedulers.Schedulers;

public class OtpActivity extends AppCompatActivity implements View.OnClickListener {

    private static final String TAG = OtpActivity.class.getSimpleName();
    private ActivityLoginAdminBinding binding;
    private Context mContext;
    private String email;
    private ProgressDialog progressDialog;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
//        setContentView(R.layout.activity_login_admin);
        binding = DataBindingUtil.setContentView(this, R.layout.activity_login_admin);
        mContext = this;

        email = getIntent().getStringExtra(Constants.EID);
        binding.toolbar.tvToolbarTitle.setText(getString(R.string.authorization));
        binding.btOtp.setOnClickListener(this);
        binding.toolbar.ivBack.setOnClickListener(this);
        binding.tvDesc.setText(getString(R.string.please_enter_the_secured_one_time_password_which_has_been_sent_to) + " " + email);

        binding.etOtp.setOnEditorActionListener(new TextView.OnEditorActionListener() {
            @Override
            public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
                boolean handled = false;
                if (actionId == EditorInfo.IME_ACTION_DONE) {
                    /* Write your logic here that will be executed when user taps next button */
                    Log.d(TAG, "onEditorAction: ");
                    KeyboardUtils.hideSoftKeyboard(mContext);
                    if (!binding.etOtp.getText().toString().trim().isEmpty()) {
                        callVerifyOtpApi();
                    } else {
                        CommonUtils.showSnackBar(getString(R.string.please_enter_valid_otp), binding.llMain);
                    }
                }
                return true;
            }
        });
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.btOtp:

//                startActivity(new Intent(mContext, MainActivity.class));
                if (!binding.etOtp.getText().toString().trim().isEmpty()) {
                    callVerifyOtpApi();
                } else {
                    CommonUtils.showSnackBar(getString(R.string.please_enter_valid_otp), binding.llMain);
                }
                break;
            case R.id.ivBack:
                onBackPressed();
                break;
        }
    }


    private void callVerifyOtpApi() {
        HashMap<String, String> map = new HashMap<>();
        map.put(Constants.EID, email.toLowerCase());
        map.put(Constants.OTP, binding.etOtp.getText().toString().trim());

        Observable<VerifyOtp> observable = RetrofitUtility.getClient().verifyOtp(map);
        progressDialog = CommonUtils.showProgressDialog(mContext);
        try {
            observable.subscribeOn(Schedulers.io())
                    .observeOn(AndroidSchedulers.mainThread())
                    .subscribe(new Observer<VerifyOtp>() {
                        @Override
                        public void onSubscribe(Disposable d) {
                            Log.d(TAG, "onSubscribe:");
                        }

                        @Override
                        public void onNext(VerifyOtp value) {
                            Log.d(TAG, "onNext:");
                            CommonUtils.dismissProgressDialog(progressDialog);
                            if (value.getStatus() != null && value.getStatus().equalsIgnoreCase("Incorrect OTP")) {
                                CommonUtils.showSnackBar(value.getStatus(), binding.llMain);
                            }
                            SharedPreferences sharedPreferences = mContext.getSharedPreferences(Constants.PARKING_PREFERENCES, MODE_PRIVATE);
                            SharedPreferences.Editor editor = sharedPreferences.edit();
                            if (value.getUserdetail() != null && value.getUserdetail().size() > 0) {
                                Userdetail userdetail = value.getUserdetail().get(0);
                                editor.putString(Constants.ID, userdetail.getId());
                                editor.putString(Constants.EID, userdetail.getEid());
                                editor.putString(Constants.ROLE, userdetail.getRole());
                                editor.putString(Constants.PARKINGID, userdetail.getParkingid());
                                editor.putString(Constants.USERNAME, userdetail.getUsername());

                                if (userdetail.getStartdate() != null) {
                                    editor.putBoolean(Constants.PARKINGOCCUPIED, false);
                                    editor.putString(Constants.STARTDATE, userdetail.getStartdate());
                                    editor.putString(Constants.ENDDATE, userdetail.getStartdate());

                                } else {
                                    editor.putBoolean(Constants.PARKINGOCCUPIED, true);
                                }
                                editor.apply();
                                editor.commit();
                                Intent intent = new Intent(mContext, MainActivity.class);
                                startActivity(intent);
                                finishAffinity();
                            } else if (value.getUserdetail() != null && value.getUserdetail().size() == 0) {
                                editor.putString(Constants.GUEST_LOGIN, "yes");
                                editor.apply();
                                editor.commit();
                                Intent intent = new Intent(mContext, MainActivity.class);
                                startActivity(intent);
                                finishAffinity();
                            }
                        }

                        @Override
                        public void onError(Throwable e) {
                            Log.d(TAG, "onError:" + e.getMessage());
                            CommonUtils.dismissProgressDialog(progressDialog);
                            CommonUtils.showSnackBar(getString(R.string.something_went_wrong), binding.llMain);
                        }

                        @Override
                        public void onComplete() {
                            Log.d(TAG, "onComplete:");
                            CommonUtils.dismissProgressDialog(progressDialog);
                        }
                    });
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public boolean dispatchTouchEvent(MotionEvent ev) {
        if(ev.getAction() == MotionEvent.ACTION_UP) {
            final View view = getCurrentFocus();

            if(view != null) {
                final boolean consumed = super.dispatchTouchEvent(ev);

                final View viewTmp = getCurrentFocus();
                final View viewNew = viewTmp != null ? viewTmp : view;

                if(viewNew.equals(view)) {
                    final Rect rect = new Rect();
                    final int[] coordinates = new int[2];

                    view.getLocationOnScreen(coordinates);

                    rect.set(coordinates[0], coordinates[1], coordinates[0] + view.getWidth(), coordinates[1] + view.getHeight());

                    final int x = (int) ev.getX();
                    final int y = (int) ev.getY();

                    if(rect.contains(x, y)) {
                        return consumed;
                    }
                }
                else if(viewNew instanceof EditText) {
                    return consumed;
                }

                final InputMethodManager inputMethodManager = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);

                inputMethodManager.hideSoftInputFromWindow(viewNew.getWindowToken(), 0);

                viewNew.clearFocus();

                return consumed;
            }
        }

        return super.dispatchTouchEvent(ev);
    }
}

package io.gonative.android.bkxxep.model.parking;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Allotment {

    public Allotment(String parkingNo, String parkingStatus) {
        this.parkingNo = parkingNo;
        this.parkingStatus = parkingStatus;
    }

    @SerializedName("_id")
    @Expose
    private String id;
    @SerializedName("parkingNo")
    @Expose
    private String parkingNo;
    @SerializedName("parkingStatus")
    @Expose
    private String parkingStatus;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getParkingNo() {
        return parkingNo;
    }

    public void setParkingNo(String parkingNo) {
        this.parkingNo = parkingNo;
    }

    public String getParkingStatus() {
        return parkingStatus;
    }

    public void setParkingStatus(String parkingStatus) {
        this.parkingStatus = parkingStatus;
    }
}

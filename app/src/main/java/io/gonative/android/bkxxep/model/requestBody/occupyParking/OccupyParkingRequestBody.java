package io.gonative.android.bkxxep.model.requestBody.occupyParking;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class OccupyParkingRequestBody {
    @SerializedName("parkingid")
    @Expose
    private String parkingid;

    public String getParkingid() {
        return parkingid;
    }

    public void setParkingid(String parkingid) {
        this.parkingid = parkingid;
    }
}

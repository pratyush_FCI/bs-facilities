package io.gonative.android.bkxxep.model.admin;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

public class Users {

    @SerializedName("userdetail")
    @Expose
    private List<Userdetail> userdetail = null;

    public List<Userdetail> getUserdetail() {
        return userdetail;
    }

    public void setUserdetail(List<Userdetail> userdetail) {
        this.userdetail = userdetail;
    }

}

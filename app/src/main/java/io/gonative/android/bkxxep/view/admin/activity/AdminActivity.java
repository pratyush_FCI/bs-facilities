package io.gonative.android.bkxxep.view.admin.activity;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.databinding.DataBindingUtil;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.util.Log;
import android.view.View;

import com.bridgestone.parkingapp.databinding.ActivityAdminBinding;


import java.util.List;

import io.gonative.android.bkxxep.model.admin.DeleteUserDetails;
import io.gonative.android.bkxxep.model.admin.Userdetail;
import io.gonative.android.bkxxep.model.admin.Users;
import io.gonative.android.bkxxep.model.requestBody.deleteUser.DeleteUserDetailsRequestBody;
import io.gonative.android.bkxxep.model.rxBusModel.RxBusModel;
import io.gonative.android.bkxxep.utility.CommonUtils;
import io.gonative.android.bkxxep.utility.NetworkUtils;
import io.gonative.android.bkxxep.utility.RetrofitUtility;
import io.gonative.android.bkxxep.utility.SharedPreferencesUtils;
import io.gonative.android.bkxxep.view.addUser.AddUserActivity;
import io.gonative.android.bkxxep.view.admin.adapter.UsersRecyclerAdapter;
import io.reactivex.Observable;
import io.reactivex.Observer;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.Disposable;
import io.reactivex.schedulers.Schedulers;



public class AdminActivity extends AppCompatActivity implements View.OnClickListener {

    private static final String TAG = AdminActivity.class.getSimpleName();
    private ActivityAdminBinding binding;
    private ProgressDialog progressDialog;
    private Context mContext;
    private List<Userdetail> usersList;
    private final Integer ADD_USER_REQUEST_CODE = 333;
    private boolean updated;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        binding = DataBindingUtil.setContentView(this, io.gonative.android.bkxxep.R.layout.activity_admin);
        mContext = this;
        RxBus.get().register(this);

        binding.toolbar.tvToolbarTitle.setText(getString(io.gonative.android.bkxxep.R.string.admin));
        binding.toolbar.ivAdd.setVisibility(View.VISIBLE);
        binding.toolbar.ivAdd.setOnClickListener(this);
        binding.toolbar.ivBack.setOnClickListener(this);

        if (NetworkUtils.isNetworkAvailable(mContext, binding.llMain)) {
            callUserDetailsApi();
        }

    }


    private void callUserDetailsApi() {
        Observable<Users> observable = RetrofitUtility.getClient().getUserDetails();
        progressDialog = CommonUtils.showProgressDialog(mContext);
        if (progressDialog != null) {
            Log.d(TAG, "callUserDetailsApi: progressDialog");
        }
        try {
            observable.subscribeOn(Schedulers.io())
                    .observeOn(AndroidSchedulers.mainThread())
                    .subscribe(new Observer<Users>() {
                        @Override
                        public void onSubscribe(Disposable d) {
                            Log.d(TAG, "onSubscribe: ");
                        }

                        @Override
                        public void onNext(Users value) {
                            Log.d(TAG, "onNext: ");
                            usersList = value.getUserdetail();
                            setAdapter();

                        }

                        @Override
                        public void onError(Throwable e) {
                            Log.d(TAG, "onError: " + e.getMessage());
                            CommonUtils.dismissProgressDialog(progressDialog);
                            CommonUtils.showSnackBar(getString(io.gonative.android.bkxxep.R.string.something_went_wrong), binding.llMain);

                        }

                        @Override
                        public void onComplete() {
                            Log.d(TAG, "onComplete: ");
                            CommonUtils.dismissProgressDialog(progressDialog);

                        }
                    });
        } catch (Exception e) {
            e.printStackTrace();
        }


    }

    private void setAdapter() {
        binding.rvUsers.setLayoutManager(new LinearLayoutManager(mContext, LinearLayoutManager.VERTICAL, false));
        binding.rvUsers.setAdapter(new UsersRecyclerAdapter(mContext, usersList));

    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case io.gonative.android.bkxxep.R.id.ivBack:
                onBackPressed();
                break;

            case io.gonative.android.bkxxep.R.id.ivAdd:

                startActivityForResult(new Intent(mContext, AddUserActivity.class), ADD_USER_REQUEST_CODE);
                break;
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        Log.d(TAG, "onActivityResult: ");

        if (resultCode == RESULT_OK) {
            if (requestCode == ADD_USER_REQUEST_CODE && NetworkUtils.isNetworkAvailable(mContext, binding.llMain)) {
                Log.d(TAG, "onActivityResult: ");
                callUserDetailsApi();
            } else if (requestCode == REQUEST_EDIT_CODE && NetworkUtils.isNetworkAvailable(mContext, binding.llMain)) {
                callUserDetailsApi();
                updated = true;
            }
        }
    }

    @Override
    protected void onDestroy() {
        RxBus.get().unregister(this);
        super.onDestroy();
    }

    @Subscribe
    public void rxCallBack(RxBusModel rxBusModel) {
        if (rxBusModel != null && rxBusModel.getTag().equalsIgnoreCase(TAG + "delete")) {
            if (NetworkUtils.isNetworkAvailable(mContext, binding.llMain)) {
                callDeleteUserApi(rxBusModel.getId());
            }
        }
    }

    private void callDeleteUserApi(String id) {
        DeleteUserDetailsRequestBody body = new DeleteUserDetailsRequestBody();
        body.setId(id);
        Observable<DeleteUserDetails> observable = RetrofitUtility.getClient().deleteUser(id);
        progressDialog = CommonUtils.showProgressDialog(mContext);
        try {
            observable.subscribeOn(Schedulers.io())
                    .observeOn(AndroidSchedulers.mainThread())
                    .subscribe(new Observer<DeleteUserDetails>() {
                        @Override
                        public void onSubscribe(Disposable d) {
                            Log.d(TAG, "onSubscribe: ");
                        }

                        @Override
                        public void onNext(DeleteUserDetails value) {
                            Log.d(TAG, "onNext: ");
                            CommonUtils.dismissProgressDialog(progressDialog);
                            CommonUtils.showSnackBar(value.getStatus(), binding.llMain);
                            if (id.equalsIgnoreCase(SharedPreferencesUtils.getUserId(mContext))) {
                                SharedPreferencesUtils.logout(mContext);

                            } else if (NetworkUtils.isNetworkAvailable(mContext, binding.llMain)) {
                                callUserDetailsApi();
                            }
                        }

                        @Override
                        public void onError(Throwable e) {
                            Log.d(TAG, "onError: " + e.getMessage());
                            CommonUtils.dismissProgressDialog(progressDialog);
                            CommonUtils.showSnackBar(getString(io.gonative.android.bkxxep.R.string.something_went_wrong), binding.llMain);

                        }

                        @Override
                        public void onComplete() {
                            Log.d(TAG, "onComplete: ");
                            CommonUtils.dismissProgressDialog(progressDialog);
                        }
                    });
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public void onBackPressed() {
        if (updated) {
            setResult(RESULT_OK);
        } else {
            setResult(RESULT_CANCELED);
        }
        super.onBackPressed();
    }
}

package io.gonative.android.bkxxep.view.admin.adapter;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.databinding.DataBindingUtil;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.ViewGroup;

import com.bridgestone.parkingapp.databinding.ItemUsersBinding;


import java.util.List;

import io.gonative.android.bkxxep.model.admin.Userdetail;
import io.gonative.android.bkxxep.model.rxBusModel.RxBusModel;
import io.gonative.android.bkxxep.utility.Constants;
import io.gonative.android.bkxxep.utility.SharedPreferencesUtils;
import io.gonative.android.bkxxep.view.admin.activity.AdminActivity;
import io.gonative.android.bkxxep.view.editUserDetails.EditUserDetailsActivity;

import static android.content.Context.MODE_PRIVATE;

public class UsersRecyclerAdapter extends RecyclerView.Adapter<UsersRecyclerAdapter.MyViewHolder> {

    public static final int REQUEST_EDIT_CODE = 121;
    private Context mContext;
    private List<Userdetail> usersList;
    private LayoutInflater layoutInflater;
    private final String TAG = UsersRecyclerAdapter.class.getSimpleName();


    public UsersRecyclerAdapter(Context mContext, List<Userdetail> usersList) {
        this.mContext = mContext;
        this.usersList = usersList;
    }

    @NonNull
    @Override
    public MyViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {

        if (layoutInflater == null) {
            layoutInflater = LayoutInflater.from(viewGroup.getContext());
        }
        ItemUsersBinding binding =
                DataBindingUtil.inflate(layoutInflater, io.gonative.android.bkxxep.R.layout.item_users, viewGroup, false);
        return new MyViewHolder(binding);
    }

    @Override
    public void onBindViewHolder(@NonNull MyViewHolder myViewHolder, int i) {
        Userdetail userdetail = usersList.get(i);
        myViewHolder.binding.setUserdetail(userdetail);

        if (userdetail.getId().equalsIgnoreCase(SharedPreferencesUtils.getUserId(mContext))) {
            SharedPreferences sharedPreferences = mContext.getSharedPreferences(Constants.PARKING_PREFERENCES, MODE_PRIVATE);
            SharedPreferences.Editor editor = sharedPreferences.edit();
            editor.putString(Constants.EID, userdetail.getEid());
            editor.putString(Constants.ROLE, userdetail.getRole());
            editor.putString(Constants.PARKINGID, userdetail.getParkingid());
            editor.putString(Constants.USERNAME, userdetail.getUsername());
            editor.apply();
            editor.commit();
        }

        myViewHolder.binding.llUser.setOnClickListener(v -> {
            Log.d(TAG, "onBindViewHolder:");
        });

        myViewHolder.binding.ivEdit.setOnClickListener(v -> {
            Log.d(TAG, "onBindViewHolder:");
            Intent intent = new Intent(mContext, EditUserDetailsActivity.class);
            intent.putExtra(Constants.ID, usersList.get(i).getId());
            intent.putExtra(Constants.EID, usersList.get(i).getEid());
            intent.putExtra("role", usersList.get(i).getRole());
            intent.putExtra(Constants.PARKINGID, usersList.get(i).getParkingid());
            intent.putExtra(Constants.USERNAME, usersList.get(i).getUsername());

            ((Activity) mContext).startActivityForResult(intent, REQUEST_EDIT_CODE);
        });

        myViewHolder.binding.ivDelete.setOnClickListener(v -> {
            Log.d(TAG, "onBindViewHolder:");
            RxBusModel rxBusModel = new RxBusModel();
            rxBusModel.setTag(AdminActivity.class.getSimpleName() + "delete");
            rxBusModel.setId(usersList.get(i).getId());
            RxBus.get().post(rxBusModel);
        });
    }

    @Override
    public int getItemCount() {
        return usersList.size();
    }


    public class MyViewHolder extends RecyclerView.ViewHolder {
        private final ItemUsersBinding binding;

        public MyViewHolder(ItemUsersBinding itembinding) {

            super(itembinding.getRoot());
            binding = itembinding;

        }
    }
}

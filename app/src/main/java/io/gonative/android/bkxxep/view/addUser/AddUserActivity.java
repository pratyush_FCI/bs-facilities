package io.gonative.android.bkxxep.view.addUser;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.databinding.DataBindingUtil;
import android.graphics.Rect;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.KeyEvent;
import android.view.MotionEvent;
import android.view.View;
import android.view.inputmethod.EditorInfo;
import android.view.inputmethod.InputMethodManager;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.TextView;


import com.bridgestone.parkingapp.databinding.ActivityAddUserBinding;


import java.util.ArrayList;
import java.util.List;

import io.gonative.android.bkxxep.model.addUser.AddUser;
import io.gonative.android.bkxxep.utility.CommonUtils;
import io.gonative.android.bkxxep.utility.Constants;
import io.gonative.android.bkxxep.utility.EmailUtils;
import io.gonative.android.bkxxep.utility.KeyboardUtils;
import io.gonative.android.bkxxep.utility.NetworkUtils;
import io.gonative.android.bkxxep.utility.RetrofitUtility;
import io.reactivex.Observable;
import io.reactivex.Observer;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.Disposable;
import io.reactivex.schedulers.Schedulers;

public class AddUserActivity extends AppCompatActivity implements View.OnClickListener {

    private static final String TAG = AddUserActivity.class.getSimpleName();
    private ActivityAddUserBinding binding;
    private Context mContext;
    private String selectedRole;
    private boolean itemSelected;
    private List<String> parkingIdList;
    private ProgressDialog progressDialog;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        binding = DataBindingUtil.setContentView(this, io.gonative.android.bkxxep.R.layout.activity_add_user);
        mContext = this;

        binding.btSubmit.setOnClickListener(this);
        binding.toolbar.ivBack.setOnClickListener(this);
        setSpinnerItems();
        binding.toolbar.tvToolbarTitle.setText(getString(io.gonative.android.bkxxep.R.string.add_user));

        binding.etParkingId.setOnEditorActionListener(new TextView.OnEditorActionListener() {
            @Override
            public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
                boolean handled = false;
                if (actionId == EditorInfo.IME_ACTION_DONE) {
                    /* Write your logic here that will be executed when user taps next button */
                    Log.d(TAG, "onEditorAction: ");
                    KeyboardUtils.hideSoftKeyboard(mContext);
                    if (verifyAddUser() && NetworkUtils.isNetworkAvailable(mContext, binding.llMain)) {
                        callAddUserApi();
                    }
                }
                return true;
            }
        });
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case io.gonative.android.bkxxep.R.id.ivBack:
                onBackPressed();
                break;
            case io.gonative.android.bkxxep.R.id.btSubmit:
                KeyboardUtils.hideSoftKeyboard(mContext);
                if (verifyAddUser() && NetworkUtils.isNetworkAvailable(mContext, binding.llMain)) {
                    callAddUserApi();
                }
                break;
        }
    }

    private void setSpinnerItems() {

        parkingIdList = new ArrayList<>();
        for (int i = 1; i < 111; i++) {
            if (i == 57) {

            } else {

                parkingIdList.add(String.valueOf(i));
            }
        }
        Log.d(TAG, "setSpinnerItems: " + parkingIdList.size());
        List<String> list = new ArrayList<String>();
        list.add(getString(io.gonative.android.bkxxep.R.string.select_role));
        list.add(getString(io.gonative.android.bkxxep.R.string.admin));
        list.add(getString(io.gonative.android.bkxxep.R.string.regular));
        ArrayAdapter<String> dataAdapter = new ArrayAdapter<String>(this,
                android.R.layout.simple_spinner_item, list);
        dataAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        binding.spRole.setAdapter(dataAdapter);
        binding.spRole.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                Log.d(TAG, "onItemSelected: " + position);
                if (position == 0) {
                    selectedRole = "";
                    if (itemSelected) {
                        CommonUtils.showSnackBar(getString(io.gonative.android.bkxxep.R.string.please_select_a_role), binding.llMain);
                    } else {
                        itemSelected = !itemSelected;
                    }
                } else {
                    selectedRole = list.get(position).toLowerCase();
                    Log.d(TAG, "setSpinnerItems: " + selectedRole);
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {
                Log.d(TAG, "onNothingSelected: ");
            }
        });
    }

    private boolean verifyAddUser() {

        if (!EmailUtils.isEmailValid(binding.etEmail.getText().toString().trim())) {
            CommonUtils.showSnackBar(getString(io.gonative.android.bkxxep.R.string.please_enter_valid_email), binding.llMain);
            return false;
        }

        if (!EmailUtils.getEmailDomain(binding.etEmail.getText().toString().trim()).equalsIgnoreCase(Constants.BRIDGESTONE_DOMAIN) &&
                !EmailUtils.getEmailDomain(binding.etEmail.getText().toString().trim()).equalsIgnoreCase(Constants.FUJITSU_DOMAIN) &&
                !EmailUtils.getEmailDomain(binding.etEmail.getText().toString().trim()).equalsIgnoreCase(Constants.FIRSTSTOP_DOMAIN)) {
            CommonUtils.showSnackBar(getString(io.gonative.android.bkxxep.R.string.please_enter_bridgestone_domain), binding.llMain);
            return false;
        }

        if (binding.etUserName.getText().toString().trim().isEmpty()) {
            CommonUtils.showSnackBar(getString(io.gonative.android.bkxxep.R.string.please_enter_valid_username), binding.llMain);
            return false;
        }

        if (selectedRole.isEmpty()) {
            CommonUtils.showSnackBar(getString(io.gonative.android.bkxxep.R.string.please_select_a_role), binding.llMain);
            return false;
        }

        if (selectedRole.equalsIgnoreCase("regular") && binding.etParkingId.getText().toString().trim().isEmpty()) {
            CommonUtils.showSnackBar(getString(io.gonative.android.bkxxep.R.string.parking_id_is_necessary_for_regular_users), binding.llMain);
            return false;
        }

        if (!binding.etParkingId.getText().toString().trim().isEmpty() && !parkingIdList.contains(binding.etParkingId.getText().toString().trim())) {
            CommonUtils.showSnackBar(getString(io.gonative.android.bkxxep.R.string.please_enter_valid_parking_id), binding.llMain);
            return false;
        }
        return true;
    }

    @Override
    public boolean dispatchTouchEvent(MotionEvent ev) {
        if (ev.getAction() == MotionEvent.ACTION_UP) {
            final View view = getCurrentFocus();

            if (view != null) {
                final boolean consumed = super.dispatchTouchEvent(ev);

                final View viewTmp = getCurrentFocus();
                final View viewNew = viewTmp != null ? viewTmp : view;

                if (viewNew.equals(view)) {
                    final Rect rect = new Rect();
                    final int[] coordinates = new int[2];

                    view.getLocationOnScreen(coordinates);

                    rect.set(coordinates[0], coordinates[1], coordinates[0] + view.getWidth(), coordinates[1] + view.getHeight());

                    final int x = (int) ev.getX();
                    final int y = (int) ev.getY();

                    if (rect.contains(x, y)) {
                        return consumed;
                    }
                } else if (viewNew instanceof EditText) {
                    return consumed;
                }

                final InputMethodManager inputMethodManager = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);

                inputMethodManager.hideSoftInputFromWindow(viewNew.getWindowToken(), 0);

                viewNew.clearFocus();

                return consumed;
            }
        }

        return super.dispatchTouchEvent(ev);
    }

    private void callAddUserApi() {
        Observable<AddUser> observable = RetrofitUtility.getClient().addNewUser(binding.etEmail.getText().toString().trim().toLowerCase(),
                binding.etUserName.getText().toString(), binding.etParkingId.getText().toString().trim(), selectedRole);
        progressDialog = CommonUtils.showProgressDialog(mContext);
        try {
            observable.subscribeOn(Schedulers.io())
                    .observeOn(AndroidSchedulers.mainThread())
                    .subscribe(new Observer<AddUser>() {
                        @Override
                        public void onSubscribe(Disposable d) {
                            Log.d(TAG, "onSubscribe: ");
                        }

                        @Override
                        public void onNext(AddUser value) {
                            Log.d(TAG, "onNext: ");
                            if (value.getStatus().equalsIgnoreCase("Record Already exist")) {
                                CommonUtils.showSnackBar(value.getStatus(), binding.llMain);
                                Log.d(TAG, "onNext: Record Already exist");
                            } else {
                                CommonUtils.dismissProgressDialog(progressDialog);
                                CommonUtils.showSnackBar(value.getStatus(), binding.llMain);
                                setResult(RESULT_OK, new Intent());
                                finish();
                            }
                        }

                        @Override
                        public void onError(Throwable e) {
                            Log.d(TAG, "onError: " + e.getMessage());
                            CommonUtils.dismissProgressDialog(progressDialog);
                            CommonUtils.showSnackBar(getString(io.gonative.android.bkxxep.R.string.something_went_wrong), binding.llMain);

                        }

                        @Override
                        public void onComplete() {
                            Log.d(TAG, "onComplete: ");
                            CommonUtils.dismissProgressDialog(progressDialog);

                        }
                    });
        } catch (Exception e) {
            e.printStackTrace();
        }
    }


    @Override
    public void onBackPressed() {
        setResult(RESULT_CANCELED, new Intent());
        super.onBackPressed();
    }

    private void hideSoftKeyboard() {
        InputMethodManager inputManager = (InputMethodManager)
                getSystemService(Context.INPUT_METHOD_SERVICE);
        if (inputManager.isAcceptingText()) {
            inputManager.hideSoftInputFromWindow(getCurrentFocus().getWindowToken(),
                    InputMethodManager.HIDE_NOT_ALWAYS);
        }
    }
}

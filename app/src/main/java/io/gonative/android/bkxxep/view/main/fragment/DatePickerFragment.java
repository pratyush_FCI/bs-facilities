package io.gonative.android.bkxxep.view.main.fragment;

import android.app.DatePickerDialog;
import android.app.Dialog;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.DialogFragment;
import android.util.Log;
import android.widget.DatePicker;


import com.bridgestone.parkingapp.R;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

public class DatePickerFragment extends DialogFragment
        implements DatePickerDialog.OnDateSetListener {

    private static final String TAG = "tag";
    private static final String DATE = "date";

    private DateInterface dateInterface;
    public static DatePickerFragment fragment;
    private String tag, date;

    public static DatePickerFragment getInstance(String tag, String date) {
        if (fragment == null) {
            fragment = new DatePickerFragment();
        }
        Bundle args = new Bundle();
        args.putString(TAG, tag);
        args.putString(DATE, date);

        fragment.setArguments(args);

        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            tag = getArguments().getString(TAG);
            date = getArguments().getString(DATE);
        }
    }


    @NonNull
    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        // Use the current date as the default date in the picker
        final Calendar c = Calendar.getInstance();
        int year = c.get(Calendar.YEAR);
        int month = c.get(Calendar.MONTH);
        int day = c.get(Calendar.DAY_OF_MONTH);

        DatePickerDialog datePickerDialog = new DatePickerDialog(getActivity(), io.gonative.android.bkxxep.R.style.DialogTheme, this, year, month, day);
        if (date.isEmpty()) {
            Log.d(TAG, "onCreateDialog: " + date);
            datePickerDialog.getDatePicker().setMinDate(System.currentTimeMillis());
        } else {
            Log.d(TAG, "onCreateDialog: " + date);
            datePickerDialog.getDatePicker().setMinDate(getDate(date));
        }

        /*SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy");
        Date d = null;
        try {
            d = sdf.parse(day + "/" + month + "/" + year);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        datePickerDialog.getDatePicker().setMinDate(d.getTime());*/

        // Create a new instance of DatePickerDialog and return it
        return datePickerDialog;
    }

    private long getDate(String dateString) {
        long startDate = 0;

        try {
            SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy");
            Date date = sdf.parse(dateString);
            startDate = date.getTime();

            Log.d(TAG, "getDate: " + startDate);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        return startDate;
    }

    public void onDateSet(DatePicker view, int year, int month, int day) {
        if (isAfterToday(year, month, day)) {

            Log.d(TAG, "day: " + day);
            Log.d(TAG, "month: " + month);
            Log.d(TAG, "year: " + year);

            String date = day + "/" + (month + 1) + "/" + year;
            dateInterface = (DateInterface) getActivity();
            dateInterface.sendDate(date, tag);
            Log.d(TAG, "onDateSet: " + date);
        } else {

        }

    }

    private boolean isAfterToday(int year, int month, int day) {
        Calendar today = Calendar.getInstance();
        Calendar myDate = Calendar.getInstance();

        myDate.set(year, month, day);

        if (myDate.before(today)) {
            return false;
        }
        return true;
    }

    public interface DateInterface {

        void sendDate(String date, String tag);
    }
}
package io.gonative.android.bkxxep.view.main.fragment;

import android.content.Context;
import android.databinding.DataBindingUtil;
import android.os.Bundle;
import android.support.v4.app.DialogFragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.bridgestone.parkingapp.R;
import com.bridgestone.parkingapp.databinding.FragmentFreeUpParkingDialogBinding;
import com.bridgestone.parkingapp.utility.CommonUtils;
import com.bridgestone.parkingapp.utility.DateUtils;

import java.text.SimpleDateFormat;

import io.gonative.android.bkxxep.utility.CommonUtils;

public class FreeUpParkingDialogFragment extends DialogFragment implements View.OnClickListener {


    public static FreeUpParkingDialogFragment fragment;
    private final String FROM = "from";
    private final String TO = "to";
    private String selectedDate;
    private FragmentFreeUpParkingDialogBinding binding;
    private SubmitDate submitDate;
    private String startDate, endDate;
    private final String TAG = FreeUpParkingDialogFragment.class.getSimpleName();


    public FreeUpParkingDialogFragment() {
        // Required empty public constructor
    }

    public static FreeUpParkingDialogFragment newInstance() {
        if (fragment == null) {

            fragment = new FreeUpParkingDialogFragment();
        }
        return fragment;
    }

    public void updateDate(String date, String tag) {

    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        binding = DataBindingUtil.inflate(
                inflater, io.gonative.android.bkxxep.R.layout.fragment_free_up_parking_dialog, container, false);
        View view = binding.getRoot();

        binding.tvFrom.setOnClickListener(this);
        binding.tvTo.setOnClickListener(this);
        binding.btSubmit.setOnClickListener(this);
        SimpleDateFormat formatter = new SimpleDateFormat("dd/MM/yyyy");
        selectedDate = endDate = startDate = formatter.format(System.currentTimeMillis());
//        String currentDate = DateUtils.getDate(System.currentTimeMillis(),"dd/mm/yyyy");
        binding.tvFrom.setText(startDate);
        binding.tvTo.setText(endDate);

        return view;
    }


    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
    }

    @Override
    public void onDetach() {
        super.onDetach();
    }

    @Override
    public void onClick(View v) {
        DatePickerFragment fragment;
        switch (v.getId()) {
            case io.gonative.android.bkxxep.R.id.tvFrom:
                fragment = DatePickerFragment.getInstance(FROM, "");
                fragment.show(getFragmentManager(), "");
                break;

            case io.gonative.android.bkxxep.R.id.btSubmit:
                submitDate = (SubmitDate) getActivity();
                if (verifyDates()) {

                    submitDate.submit(startDate, endDate);
                    getDialog().dismiss();
                }
                break;

            case io.gonative.android.bkxxep.R.id.tvTo:
                Log.d(TAG, "onClick: " + binding.tvFrom.getText().toString());
                if (selectedDate != null && !selectedDate.isEmpty()) {

                    fragment = DatePickerFragment.getInstance(TO, selectedDate);
                    fragment.show(getFragmentManager(), "");
                } else {
                    CommonUtils.showSnackBar(getString(io.gonative.android.bkxxep.R.string.please_select_from_date_first), binding.llMain);
                }
                break;
        }
    }

    private boolean verifyDates() {

        if (startDate.isEmpty()) {
            CommonUtils.showSnackBar(getString(io.gonative.android.bkxxep.R.string.please_enter_satrt_date), binding.llMain);
            return false;
        }

        if (endDate.isEmpty()) {
            CommonUtils.showSnackBar(getString(io.gonative.android.bkxxep.R.string.please_enter_end_date), binding.llMain);
            return false;
        }

        return true;
    }

    public interface SubmitDate {
        public void submit(String startDate, String endDate);
    }

    public void setdate(String date, String tag) {
        if (tag.equalsIgnoreCase(FROM)) {
            binding.tvFrom.setText(date);
            selectedDate = date;
            binding.tvTo.setText("");
            startDate = date;
            endDate = "";

        } else if (tag.equalsIgnoreCase(TO)) {
            binding.tvTo.setText(date);
            endDate = date;
        }
    }
}

package io.gonative.android.bkxxep.view.parking;

import android.app.ProgressDialog;
import android.content.Context;
import android.graphics.Color;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.DisplayMetrics;
import android.util.Log;
import android.util.TypedValue;
import android.view.Gravity;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.bridgestone.parkingapp.R;
import com.bridgestone.parkingapp.application.ParkingApplication;
import com.bridgestone.parkingapp.model.parking.Allotment;
import com.bridgestone.parkingapp.model.parking.ParkingDetails;
import com.bridgestone.parkingapp.utility.CommonUtils;
import com.bridgestone.parkingapp.utility.NetworkUtils;
import com.bridgestone.parkingapp.utility.RetrofitUtility;
import com.bridgestone.parkingapp.view.main.activity.MainActivity;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.io.InputStream;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.HashMap;
import java.util.List;

import io.gonative.android.bkxxep.application.ParkingApplication;
import io.gonative.android.bkxxep.model.parking.Allotment;
import io.gonative.android.bkxxep.model.parking.ParkingDetails;
import io.gonative.android.bkxxep.utility.CommonUtils;
import io.gonative.android.bkxxep.utility.NetworkUtils;
import io.gonative.android.bkxxep.utility.RetrofitUtility;
import io.socket.client.Socket;

import io.reactivex.Observable;
import io.reactivex.Observer;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.CompositeDisposable;
import io.reactivex.disposables.Disposable;
import io.reactivex.schedulers.Schedulers;
import retrofit2.Response;

public class ParkingActivity extends AppCompatActivity implements View.OnClickListener {
    ViewGroup layout;

    private final String TAG = ParkingActivity.class.getSimpleName();

    private List<Allotment> localParkingList;
    private List<Allotment> apiParkingList;

    List<TextView> seatViewList = new ArrayList<>();
    HashMap<String, String> parkingMap = new HashMap<>();
    int seatSize, seatHeight;
    private ImageView ivBack;
    private CompositeDisposable _disposables;
    private Context mContext;
    private ProgressDialog progressDialog;

    private final String RED = "red";
    private final String GREY = "grey";
    private final String GREEN = "green";
    private final String YELLOW = "yellow";
    private final String PURPLE = "purple";

    private final String NO_PARKING = "np";
    private ProgressBar progressBar;
    private RelativeLayout rlMain;
    private float textSize;
    private TextView tvToolbarTitle;

    private String severIp = "192.168.0.2";
    private long startTime = 0l;
    private int serverPort = 1234;
    private Socket mSocket;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(io.gonative.android.bkxxep.R.layout.activity_parking);
//        ActivityParkingBinding binding = DataBindingUtil.setContentView(this, R.layout.activity_login);


        mContext = this;


        initialize();


        //   mSocket.on("userList", onLogin);
        /*if (CommonUtils.isNetworkAvailable(mContext)) {
            callParkingDetailsApi();
        }*/

        getLocalListFromJson();


    }

    private io.socket.emitter.Emitter.Listener onParkingUpdate = new io.socket.emitter.Emitter.Listener() {
        @Override
        public void call(final Object... args) {
            runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    JSONObject data = (JSONObject) args[0];
                    String username;
                    String color;
                    try {
                        username = data.getString("parkingNo");
                        color = data.getString("parkingStatus");
                        Log.d(TAG, "run: " + username);
                        Log.d(TAG, "run: " + color);
                        updateLayout(Integer.parseInt(username), color);
                    } catch (JSONException e) {
                        Log.e(TAG, e.getMessage());
                        return;
                    }


                }
            });
        }
    };


    private io.socket.emitter.Emitter.Listener onConnect = new io.socket.emitter.Emitter.Listener() {
        @Override
        public void call(Object... args) {

        }


    };

    private io.socket.emitter.Emitter.Listener onLogin;

    {
        onLogin = new io.socket.emitter.Emitter.Listener() {
            @Override
            public void call(Object... args) {


            }
        };
    }

    private void initialize() {
        layout = findViewById(io.gonative.android.bkxxep.R.id.layoutParking);
        ivBack = findViewById(io.gonative.android.bkxxep.R.id.ivBack);
        rlMain = findViewById(io.gonative.android.bkxxep.R.id.rlMain);
        tvToolbarTitle = findViewById(io.gonative.android.bkxxep.R.id.tvToolbarTitle);

//        progressBar = findViewById(R.id.progress_circular);
        ivBack.setOnClickListener(this);
        _disposables = new CompositeDisposable();
        localParkingList = new ArrayList<>();
        apiParkingList = new ArrayList<>();
        DisplayMetrics displayMetrics = new DisplayMetrics();
        getWindowManager().getDefaultDisplay().getMetrics(displayMetrics);
        int width = displayMetrics.widthPixels;

        double density = displayMetrics.density * 160;
        double x = Math.pow(displayMetrics.widthPixels / density, 2);
        double y = Math.pow(displayMetrics.heightPixels / density, 2);
        double screenInches = Math.sqrt(x + y);
        if (screenInches < 4.5) {
            textSize = 11;
        } else if (screenInches >= 4.5 && screenInches <= 6.7) {
            textSize = 14;
        } else {
            textSize = 17;
        }
        Log.d(TAG, "inch" + screenInches);
        seatSize = width / 16;
        seatHeight = (int) (seatSize * 1.5);
        Log.d(TAG, "initialize parking: " + seatSize);
        Log.d(TAG, "initialize text: " + textSize);

        tvToolbarTitle.setText(getString(io.gonative.android.bkxxep.R.string.bseu_parking));
    }

    private void createParkingUI() {
        LinearLayout layoutSeat = new LinearLayout(this);
        LinearLayout.LayoutParams params = new LinearLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT);
        layoutSeat.setOrientation(LinearLayout.VERTICAL);
        layoutSeat.setLayoutParams(params);
//        layoutSeat.setPadding(8 * seatGaping, 8 * seatGaping, 8 * seatGaping, 8 * seatGaping);
        layout.addView(layoutSeat);

        LinearLayout layout;
        layout = new LinearLayout(this);
        layout.setOrientation(LinearLayout.HORIZONTAL);
        layoutSeat.addView(layout);
//        int count = 0;


        for (int index = 0; index < localParkingList.size(); index++) {
            Allotment parking = localParkingList.get(index);
            if (localParkingList.get(index).getParkingNo().equalsIgnoreCase("/")) {
                layout = new LinearLayout(this);
                layout.setOrientation(LinearLayout.HORIZONTAL);
                layoutSeat.addView(layout);
            } else if (!parking.getParkingNo().isEmpty() && !parking.getParkingNo().equalsIgnoreCase(NO_PARKING) && parking.getParkingStatus().equalsIgnoreCase(RED)) {
//                count++;
                TextView view = new TextView(this);
                LinearLayout.LayoutParams layoutParams = new LinearLayout.LayoutParams(seatSize, seatHeight);
//                layoutParams.setMargins(seatGaping, seatGaping, seatGaping, seatGaping);
                view.setLayoutParams(layoutParams);
//                view.setPadding(0, 0, 0, 2 * seatGaping);
                view.setId(Integer.parseInt(parking.getParkingNo()));
                view.setGravity(Gravity.CENTER);
                view.setBackgroundResource(io.gonative.android.bkxxep.R.drawable.red_parking);
                view.setTextColor(Color.WHITE);
                view.setText(parking.getParkingNo());
                view.setTextSize(TypedValue.COMPLEX_UNIT_SP, textSize);
//                view.setTextSize(getResources().getDimension(R.dimen._8sdp));
//////                view.setMaxLines(1);

                layout.addView(view);
                seatViewList.add(view);
//                view.setOnClickListener(this);
                parkingMap.put(parking.getParkingNo(), parking.getParkingStatus());

            } else if (!parking.getParkingNo().isEmpty() && !parking.getParkingNo().equalsIgnoreCase(NO_PARKING) && parking.getParkingStatus().equalsIgnoreCase(GREY)) {
//                count++;
                TextView view = new TextView(this);
                LinearLayout.LayoutParams layoutParams = new LinearLayout.LayoutParams(seatSize, seatHeight);
//                layoutParams.setMargins(seatGaping, seatGaping, seatGaping, seatGaping);
                view.setLayoutParams(layoutParams);
//                view.setPadding(0, 0, 0, 2 * seatGaping);
                view.setId(Integer.parseInt(parking.getParkingNo()));
                view.setGravity(Gravity.CENTER);
                view.setBackgroundResource(io.gonative.android.bkxxep.R.drawable.grey_parking);
                view.setTextColor(Color.WHITE);
                view.setText(parking.getParkingNo());
//                view.setMaxLines(1);
                view.setTextSize(TypedValue.COMPLEX_UNIT_SP, textSize);
//                view.setTextSize(getResources().getDimension(R.dimen._8sdp));

                layout.addView(view);
                seatViewList.add(view);
                parkingMap.put(parking.getParkingNo(), parking.getParkingStatus());
//                view.setOnClickListener(this);


            } else if (!parking.getParkingNo().isEmpty() && !parking.getParkingNo().equalsIgnoreCase(NO_PARKING) && parking.getParkingStatus().equalsIgnoreCase(GREEN)) {
//                count++;
                TextView view = new TextView(this);
                LinearLayout.LayoutParams layoutParams = new LinearLayout.LayoutParams(seatSize, seatHeight);
//                layoutParams.setMargins(seatGaping, seatGaping, seatGaping, seatGaping);
                view.setLayoutParams(layoutParams);
//                view.setPadding(0, 0, 0, 2 * seatGaping);
                view.setId(Integer.parseInt(parking.getParkingNo()));
                view.setGravity(Gravity.CENTER);
                view.setBackgroundResource(io.gonative.android.bkxxep.R.drawable.green_parking);
                view.setText(parking.getParkingNo());
//                view.setMaxLines(1);
                view.setTextSize(TypedValue.COMPLEX_UNIT_SP, textSize);
//                view.setTextSize(getResources().getDimension(R.dimen._8sdp));

                view.setTextColor(Color.BLACK);
                layout.addView(view);
                seatViewList.add(view);
                parkingMap.put(parking.getParkingNo(), parking.getParkingStatus());
//                view.setOnClickListener(this);

            } else if (!parking.getParkingNo().isEmpty() && !parking.getParkingNo().equalsIgnoreCase(NO_PARKING) && parking.getParkingStatus().equalsIgnoreCase(YELLOW)) {
//                count++;
                TextView view = new TextView(this);
                LinearLayout.LayoutParams layoutParams = new LinearLayout.LayoutParams(seatSize, seatHeight);
//                layoutParams.setMargins(seatGaping, seatGaping, seatGaping, seatGaping);
                view.setLayoutParams(layoutParams);
//                view.setPadding(0, 0, 0, 2 * seatGaping);
                view.setId(Integer.parseInt(parking.getParkingNo()));
                view.setGravity(Gravity.CENTER);
                view.setBackgroundResource(io.gonative.android.bkxxep.R.drawable.yellow_parking);
                view.setText(parking.getParkingNo());
//                view.setMaxLines(1);
                view.setTextSize(TypedValue.COMPLEX_UNIT_SP, textSize);
//                view.setTextSize(getResources().getDimension(R.dimen._8sdp));

                view.setTextColor(Color.BLACK);
                layout.addView(view);
                seatViewList.add(view);
                parkingMap.put(parking.getParkingNo(), parking.getParkingStatus());
//                view.setOnClickListener(this);

            } else if (!parking.getParkingNo().isEmpty() && !parking.getParkingNo().equalsIgnoreCase(NO_PARKING) && parking.getParkingStatus().equalsIgnoreCase(PURPLE)) {
//                count++;
                TextView view = new TextView(this);
                LinearLayout.LayoutParams layoutParams = new LinearLayout.LayoutParams(seatSize, seatHeight);
//                layoutParams.setMargins(seatGaping, seatGaping, seatGaping, seatGaping);
                view.setLayoutParams(layoutParams);
//                view.setPadding(0, 0, 0, 2 * seatGaping);
                view.setId(Integer.parseInt(parking.getParkingNo()));
                view.setGravity(Gravity.CENTER);
                view.setBackgroundResource(io.gonative.android.bkxxep.R.drawable.purple_parking);
                view.setText(parking.getParkingNo());
//                view.setMaxLines(1);
                view.setTextSize(TypedValue.COMPLEX_UNIT_SP, textSize);
//                view.setTextSize(getResources().getDimension(R.dimen._8sdp));

                view.setTextColor(Color.BLACK);
                layout.addView(view);
                seatViewList.add(view);
                parkingMap.put(parking.getParkingNo(), parking.getParkingStatus());
//                view.setOnClickListener(this);

            } else if (parking.getParkingNo().equalsIgnoreCase(NO_PARKING)) {
//                count++;
                TextView view = new TextView(this);
                LinearLayout.LayoutParams layoutParams = new LinearLayout.LayoutParams(seatSize, seatHeight);
//                layoutParams.setMargins(seatGaping, seatGaping, seatGaping, seatGaping);
                view.setLayoutParams(layoutParams);
//                view.setPadding(0, 0, 0, 2 * seatGaping);
                view.setGravity(Gravity.CENTER);
                view.setBackgroundResource(io.gonative.android.bkxxep.R.drawable.dark_gray_parking);
                view.setText("");
                view.setTextSize(TypedValue.COMPLEX_UNIT_SP, textSize);
//                view.setTextSize(getResources().getDimension(R.dimen._8sdp));
//                view.setMaxLines(1);

                view.setTextColor(Color.WHITE);
                layout.addView(view);
                seatViewList.add(view);
//                view.setOnClickListener(this);

            } else if (parking.getParkingNo().isEmpty()) {
                TextView view = new TextView(this);
                LinearLayout.LayoutParams layoutParams = new LinearLayout.LayoutParams(seatSize, seatHeight);
//                layoutParams.setMargins(seatGaping, seatGaping, seatGaping, seatGaping);
                view.setLayoutParams(layoutParams);
                view.setBackgroundColor(Color.TRANSPARENT);
                view.setText("");
                layout.addView(view);
            } else if (parking.getParkingNo().equalsIgnoreCase("entry")) {
                ImageView view = new ImageView(this);
                LinearLayout.LayoutParams layoutParams = new LinearLayout.LayoutParams(seatSize, seatHeight);
//                layoutParams.setMargins(seatGaping, seatGaping, seatGaping, seatGaping);
                view.setLayoutParams(layoutParams);
                view.setBackgroundColor(Color.TRANSPARENT);
                view.setImageDrawable(ContextCompat.getDrawable(mContext, io.gonative.android.bkxxep.R.drawable.entry));
                layout.addView(view);
            } else if (parking.getParkingNo().equalsIgnoreCase("exit")) {
                ImageView view = new ImageView(this);
                LinearLayout.LayoutParams layoutParams = new LinearLayout.LayoutParams(seatSize, seatHeight);
//                layoutParams.setMargins(seatGaping, seatGaping, seatGaping, seatGaping);
                view.setLayoutParams(layoutParams);
                view.setBackgroundColor(Color.TRANSPARENT);
                view.setImageDrawable(ContextCompat.getDrawable(mContext, io.gonative.android.bkxxep.R.drawable.exit));
                layout.addView(view);
            } else if (parking.getParkingNo().equalsIgnoreCase("exit1")) {
                TextView view = new TextView(this);
                LinearLayout.LayoutParams layoutParams = new LinearLayout.LayoutParams(seatSize * 3, seatHeight);
//                layoutParams.setMargins(seatGaping, seatGaping, seatGaping, seatGaping);
                view.setLayoutParams(layoutParams);
                view.setBackgroundColor(Color.TRANSPARENT);
                view.setText("EXIT");
                view.setTextColor(ContextCompat.getColor(mContext, io.gonative.android.bkxxep.R.color.black));
                view.setTextSize(TypedValue.COMPLEX_UNIT_SP, (float) (textSize * 1.5));
                view.setGravity(Gravity.LEFT);

                layout.addView(view);
            } else if (parking.getParkingNo().equalsIgnoreCase("entry1")) {
                TextView view = new TextView(this);
                LinearLayout.LayoutParams layoutParams = new LinearLayout.LayoutParams(seatSize * 4, seatHeight);
//                layoutParams.setMargins(seatGaping, seatGaping, seatGaping, seatGaping);
                view.setLayoutParams(layoutParams);
                view.setBackgroundColor(Color.TRANSPARENT);
                view.setText("ENTRY");
                view.setGravity(Gravity.LEFT);
                view.setTextColor(ContextCompat.getColor(mContext, io.gonative.android.bkxxep.R.color.black));
                view.setTextSize(TypedValue.COMPLEX_UNIT_SP, (float) (textSize * 1.5));

                layout.addView(view);
            }
        }
        if (NetworkUtils.isNetworkAvailable(mContext, rlMain)) {
            callParkingDetailsApi();
        }
    }

    private void callParkingDetailsApi() {
        Calendar cal = Calendar.getInstance();

// go back two days
        cal.add(Calendar.DAY_OF_YEAR, -1);
        SimpleDateFormat formatter = new SimpleDateFormat("dd/MM/yyyy");
        String startDate = formatter.format(cal.getTime());
        Log.d(TAG, "callParkingDetailsApi: " + startDate);
        Observable<Response<ParkingDetails>> observable = RetrofitUtility.getClient().getParkingDetails(startDate);
        progressDialog = CommonUtils.showProgressDialog(mContext);
        if (progressDialog != null) {
            Log.d(TAG, "callParkingDetailsApi: progressDialog");
        }
        try {
            observable.subscribeOn(Schedulers.io())
                    .observeOn(AndroidSchedulers.mainThread())
                    .subscribe(new Observer<Response<ParkingDetails>>() {
                        @Override
                        public void onSubscribe(Disposable d) {
                            Log.d(TAG, "onSubscribe: ");
                        }

                        @Override
                        public void onNext(Response<ParkingDetails> value) {
                            Log.d(TAG, "onNext: ");
                            if (value != null) {
                                localParkingList.clear();
                                localParkingList = value.body().getAllotment();
                                updateParkingUI();
                                initSocket();
                            }
                        }

                        @Override
                        public void onError(Throwable e) {
                            Log.d(TAG, "onError: " + e.getMessage());
                            CommonUtils.dismissProgressDialog(progressDialog);
                            CommonUtils.showSnackBar(getString(io.gonative.android.bkxxep.R.string.something_went_wrong), layout);


                        }

                        @Override
                        public void onComplete() {
                            Log.d(TAG, "onComplete: ");
                            CommonUtils.dismissProgressDialog(progressDialog);

                        }
                    });
        } catch (Exception e) {
            e.printStackTrace();
        }


    }

    private void initSocket() {
        ParkingApplication app = (ParkingApplication) getApplication();
        mSocket = app.getSocket();
        mSocket.on(Socket.EVENT_CONNECT, onConnect);
        mSocket.on("parkingUpdate", onParkingUpdate);
        mSocket.connect();
    }

    private void updateParkingUI() {

        for (Allotment allotment : localParkingList) {
            int id = Integer.parseInt(allotment.getParkingNo());
            Log.d(TAG, "updateParkingUI: " + id);

            if (allotment.getParkingStatus().equalsIgnoreCase(RED)) {
                findViewById(id).setBackgroundResource(io.gonative.android.bkxxep.R.drawable.red_parking);
            } else if (allotment.getParkingStatus().equalsIgnoreCase(GREEN)) {
                findViewById(id).setBackgroundResource(io.gonative.android.bkxxep.R.drawable.green_parking);
            } else if (allotment.getParkingStatus().equalsIgnoreCase(GREY)) {
                findViewById(id).setBackgroundResource(io.gonative.android.bkxxep.R.drawable.grey_parking);
            }
        }

        CommonUtils.dismissProgressDialog(progressDialog);
    }

    private void getLocalListFromJson() {
        try {
            JSONObject obj = new JSONObject(loadJSONFromAsset());
            JSONArray m_jArry = obj.getJSONArray("Allotment");

            for (int i = 0; i < m_jArry.length(); i++) {
                JSONObject jo_inside = m_jArry.getJSONObject(i);
                String parkingStatus = jo_inside.getString("parkingStatus");
                String parkingNo = jo_inside.getString("parkingNo");

                localParkingList.add(new Allotment(parkingNo, parkingStatus));
            }
            Log.d(TAG, "getLocalListFromJson: " + localParkingList.size());
            createParkingUI();


        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    public String loadJSONFromAsset() {
        String json = null;
        try {
            InputStream is = getAssets().open("Viewparkingggg.json");
            int size = is.available();
            byte[] buffer = new byte[size];
            is.read(buffer);
            is.close();
            json = new String(buffer, "UTF-8");
        } catch (IOException ex) {
            ex.printStackTrace();
            return null;
        }
        return json;
    }

    @Override
    protected void onResume() {
        super.onResume();
    }

    @Override
    public void onClick(View view) {
        int id = view.getId();
        if (id == io.gonative.android.bkxxep.R.id.ivBack) {
            onBackPressed();
        }
//        } else {
//            updateLayout(id, "green");
//        }
        Log.d(TAG, "onClick: " + id);
    }

    private void updateLayout(int id, String color) {
        View view = findViewById(id);
        Log.d(TAG, "updateLayout: " + id + "   " + color);
        if (color.equalsIgnoreCase(GREEN)) {
            view.setBackgroundResource(io.gonative.android.bkxxep.R.drawable.green_parking);
            Log.d(TAG, "updateLayout: green");

        } else if (color.equalsIgnoreCase(RED)) {
            view.setBackgroundResource(io.gonative.android.bkxxep.R.drawable.red_parking);

        } else if (color.equalsIgnoreCase(YELLOW)) {
            view.setBackgroundResource(io.gonative.android.bkxxep.R.drawable.yellow_parking);

        } else if (color.equalsIgnoreCase(GREY)) {
            view.setBackgroundResource(io.gonative.android.bkxxep.R.drawable.grey_parking);

        } else if (color.equalsIgnoreCase(PURPLE)) {
            view.setBackgroundResource(io.gonative.android.bkxxep.R.drawable.purple_parking);

        }
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
    }

    @Override
    protected void onDestroy() {
        if (_disposables != null && !_disposables.isDisposed()) {
            _disposables.dispose();
        }


        if (mSocket != null && mSocket.connected()) {
            mSocket.disconnect();
            mSocket.off(Socket.EVENT_CONNECT, onConnect);
            mSocket.off("parkingUpdate", onParkingUpdate);
        }


        super.onDestroy();
    }

/*    public void Connect(String ip, int port) {
        severIp = ip;
        serverPort = port;
        new Thread(new ConnectRunnable()).start();
    }*/


/*
    public class ConnectRunnable implements Runnable {

        public void run() {
            try {

                Log.d(TAG, "C: Connecting...");

                InetAddress serverAddr = InetAddress.getByName(severIp);
                startTime = System.currentTimeMillis();

                //Create a new instance of Socket
                connectionSocket = new Socket();

                //Start connecting to the server with 5000ms timeout
                //This will block the thread until a connection is established
                connectionSocket.connect(new InetSocketAddress(serverAddr, serverPort), 5000);

                long time = System.currentTimeMillis() - startTime;
                Log.d(TAG, "Connected! Current duration: " + time + "ms");
            } catch (Exception e) {
                //Catch the exception that socket.connect might throw
            }
            Log.d(TAG, "Connetion thread stopped");
        }
    }
*/

}



package io.gonative.android.bkxxep.model.freeParking;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class FreeParking {

    @SerializedName("userdetail")
    @Expose
    private Userdetail userdetail;

    public Userdetail getUserdetail() {
        return userdetail;
    }

    public void setUserdetail(Userdetail userdetail) {
        this.userdetail = userdetail;
    }
}

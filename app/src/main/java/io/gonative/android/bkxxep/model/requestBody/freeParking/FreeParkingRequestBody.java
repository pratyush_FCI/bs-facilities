package io.gonative.android.bkxxep.model.requestBody.freeParking;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class FreeParkingRequestBody {
    @SerializedName("parkingid")
    @Expose
    private String parkingid;

    @SerializedName("startdate")
    @Expose
    private String startdate;

    @SerializedName("enddate")
    @Expose
    private String enddate;

    public String getParkingid() {
        return parkingid;
    }

    public void setParkingid(String parkingid) {
        this.parkingid = parkingid;
    }

    public String getStartdate() {
        return startdate;
    }

    public void setStartdate(String startdate) {
        this.startdate = startdate;
    }

    public String getEnddate() {
        return enddate;
    }

    public void setEnddate(String enddate) {
        this.enddate = enddate;
    }
}

package io.gonative.android.bkxxep.model.otp;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Userdetail {

    @SerializedName("_id")
    @Expose
    private String id;
    @SerializedName("eid")
    @Expose
    private String eid;
    @SerializedName("username")
    @Expose
    private String username;
    @SerializedName("parkingid")
    @Expose
    private String parkingid;
    @SerializedName("role")
    @Expose
    private String role;

    @SerializedName("enddate")
    @Expose
    private String enddate;

    @SerializedName("startdate")
    @Expose
    private String startdate;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getEid() {
        return eid;
    }

    public void setEid(String eid) {
        this.eid = eid;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getParkingid() {
        return parkingid;
    }

    public void setParkingid(String parkingid) {
        this.parkingid = parkingid;
    }

    public String getRole() {
        return role;
    }

    public void setRole(String role) {
        this.role = role;
    }

    public String getEnddate() {
        return enddate;
    }

    public void setEnddate(String enddate) {
        this.enddate = enddate;
    }

    public String getStartdate() {
        return startdate;
    }

    public void setStartdate(String startdate) {
        this.startdate = startdate;
    }
}

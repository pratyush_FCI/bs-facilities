package io.gonative.android.bkxxep.model.requestBody.deleteUser;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class DeleteUserDetailsRequestBody {
    @SerializedName("uid")
    @Expose
    private String id;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }
}

package io.gonative.android.bkxxep.model.otp;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

public class VerifyOtp {

    @SerializedName("status")
    @Expose
    private String status;

    @SerializedName("userdetail")
    @Expose
    private List<Userdetail> userdetail = null;

    public List<Userdetail> getUserdetail() {
        return userdetail;
    }

    public void setUserdetail(List<Userdetail> userdetail) {
        this.userdetail = userdetail;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }
}

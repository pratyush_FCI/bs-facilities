package io.gonative.android.bkxxep.utility;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;


import io.gonative.android.bkxxep.view.login.LoginActivity;

import static android.content.Context.MODE_PRIVATE;

public class SharedPreferencesUtils {


    public static String getRole(Context mContext) {
        SharedPreferences sharedPreferences = mContext.getSharedPreferences(Constants.PARKING_PREFERENCES, MODE_PRIVATE);
        return sharedPreferences.getString(Constants.ROLE, "");
    }

    public static String getParkingId(Context mContext) {
        SharedPreferences sharedPreferences = mContext.getSharedPreferences(Constants.PARKING_PREFERENCES, MODE_PRIVATE);
        return sharedPreferences.getString(Constants.PARKINGID, "");
    }

    public static String getUserId(Context mContext) {
        SharedPreferences sharedPreferences = mContext.getSharedPreferences(Constants.PARKING_PREFERENCES, MODE_PRIVATE);
        return sharedPreferences.getString(Constants.ID, "");
    }

    public static String getIsGuestUser(Context mContext) {
        SharedPreferences sharedPreferences = mContext.getSharedPreferences(Constants.PARKING_PREFERENCES, MODE_PRIVATE);
        return sharedPreferences.getString(Constants.GUEST_LOGIN, "");
    }

    public static String getUsername(Context mContext) {
        SharedPreferences sharedPreferences = mContext.getSharedPreferences(Constants.PARKING_PREFERENCES, MODE_PRIVATE);
        return sharedPreferences.getString(Constants.USERNAME, "");
    }

    public static String getStartDate(Context mContext) {
        SharedPreferences sharedPreferences = mContext.getSharedPreferences(Constants.PARKING_PREFERENCES, MODE_PRIVATE);
        return sharedPreferences.getString(Constants.STARTDATE, "");
    }

    public static String getEndDate(Context mContext) {
        SharedPreferences sharedPreferences = mContext.getSharedPreferences(Constants.PARKING_PREFERENCES, MODE_PRIVATE);
        return sharedPreferences.getString(Constants.ENDDATE, "");
    }

    public static boolean getIfParkingOccupied(Context mContext) {
        SharedPreferences sharedPreferences = mContext.getSharedPreferences(Constants.PARKING_PREFERENCES, MODE_PRIVATE);
        return sharedPreferences.getBoolean(Constants.PARKINGOCCUPIED, false);
    }

    public static void logout(Context mContext) {
        SharedPreferences sharedPreferences = mContext.getSharedPreferences(Constants.PARKING_PREFERENCES, MODE_PRIVATE);
        SharedPreferences.Editor editor = sharedPreferences.edit();
        editor.putString(Constants.ID, "");
        editor.putString(Constants.EID, "");
        editor.putString(Constants.ROLE, "");
        editor.putString(Constants.PARKINGID, "");
        editor.putString(Constants.USERNAME, "");
        editor.putString(Constants.GUEST_LOGIN, "");
        editor.apply();
        editor.commit();

        Intent intent = new Intent(mContext, LoginActivity.class);
        mContext.startActivity(intent);
        ((Activity) mContext).finishAffinity();
    }

}

package io.gonative.android.bkxxep.utility;

import android.content.Context;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.view.View;


public class NetworkUtils {

    public static boolean isNetworkAvailable(Context context, View view) {
        ConnectivityManager connectivity = (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);

        if (connectivity == null) {
            return false;
        } else {
            NetworkInfo activeNetworkInfo = connectivity.getActiveNetworkInfo();
            if (activeNetworkInfo != null && activeNetworkInfo.isConnected()) {
                return true;
            } else {
                CommonUtils.showSnackBar(context.getString(io.gonative.android.bkxxep.R.string.cant_connect_to_internet), view);
                return false;
            }
        }
    }
}

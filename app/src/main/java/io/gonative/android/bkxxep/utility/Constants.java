package io.gonative.android.bkxxep.utility;

public class Constants {

    public static final String PARKING_PREFERENCES = "MyParkingPrefs";
    //    public static final String BASE_URL = "https://9d2bec5d.ngrok.io";
    public static final String BASE_URL = "https://as-e-ne-p-parking002.azurewebsites.net";
    // public static final String BASE_URL = "https://bfbf8999.ngrok.io";

    public static final String BRIDGESTONE_DOMAIN = "bridgestone.eu";
    public static final String FUJITSU_DOMAIN = "in.fujitsu.com";
    public static final String FIRSTSTOP_DOMAIN = "firststop.eu";

    //login keys
    public static final String EID = "eid";
    public static final String PASSWORD = "password";

    //add user
    public static final String USERNAME = "username";
    public static final String ROLE = "role";
    public static final String PARKINGID = "parkingid";


    //otp
    public static final String OTP = "otp";

    //sharedpreferences
    public static final String ID = "id";
    public static final String PARKINGOCCUPIED = "parkingOccupied";
    public static final String GUEST_LOGIN = "guest_login";


    //Free Parking
    public static final String STARTDATE = "startdate";
    public static final String ENDDATE = "enddate";
}

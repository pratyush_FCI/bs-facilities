package io.gonative.android.bkxxep.model.parking;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

public class ParkingDetails {

    @SerializedName("Allotment")
    @Expose
    private List<Allotment> allotment = null;

    public List<Allotment> getAllotment() {
        return allotment;
    }

    public void setAllotment(List<Allotment> allotment) {
        this.allotment = allotment;
    }
}
